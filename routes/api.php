<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

//-------------------------------------------------------------------------
// Modulo usuario
//-------------------------------------------------------------------------
Route::group(['prefix' => 'user'], function () {
    Route::post('/login', 'api\UsersController@login');

    Route::group(['middleware' => 'user.auth'], function () {
        Route::get('/logout', 'api\UsersController@logout');
        Route::get('/current', 'api\UsersController@current');
        Route::post('/change-password', 'api\UsersController@changePassword');

        Route::get('/', 'api\UsersController@index')
            ->middleware('permission.auth:users_view');

        Route::post('/', 'api\UsersController@save')
            ->middleware('permission.auth:users_create,users_edit');

        Route::get('{id}', 'api\UsersController@get')
            ->where('id', '[0-9]+');

        Route::delete('{id}', 'api\UsersController@destroy')
            ->where('id', '[0-9]+')
            ->middleware('permission.auth:users_delete');
    });
});

Route::group(['middleware' => 'user.auth'], function () {
    //-------------------------------------------------------------------------
    // Modulo menú
    //-------------------------------------------------------------------------
    Route::group(['prefix' => 'menu'], function () {
        Route::get('/tree', 'api\MenuController@tree');

        Route::get('/', 'api\MenuController@index')
            ->middleware('permission.auth:menu_view');

        Route::post('/', 'api\MenuController@save')
            ->middleware('permission.auth:menu_create,menu_edit');

        Route::get('{id}', 'api\MenuController@get')
            ->middleware('permission.auth:menu_view')
            ->where('id', '[0-9]+');

        Route::delete('{id}', 'api\MenuController@destroy')
            ->where('id', '[0-9]+')
            ->middleware('permission.auth:menu_delete');

        Route::get('list', 'api\MenuController@listIndex');
    });

    //-------------------------------------------------------------------------
    // Modulo roles
    //-------------------------------------------------------------------------
    Route::group(['prefix' => 'role'], function () {
        Route::get('/', 'api\RolesController@index')
            ->middleware('permission.auth:roles_view');

        Route::post('/', 'api\RolesController@save')
            ->middleware('permission.auth:roles_create,roles_edit');

        Route::get('{id}', 'api\RolesController@get')
            ->middleware('permission.auth:roles_view')
            ->where('id', '[0-9]+');

        Route::delete('{id}', 'api\RolesController@destroy')
            ->where('id', '[0-9]+')
            ->middleware('permission.auth:roles_delete');

        Route::get('list', 'api\RolesController@listIndex');

        Route::group(['middleware' => 'permission.auth:roles_actions'], function () {
            Route::get('{id}/actions', 'api\RolesController@actions');
            Route::post('{id}/actions', 'api\RolesController@saveActions');
        });
    });

    //-------------------------------------------------------------------------
    // Modulo acciones
    //-------------------------------------------------------------------------
    Route::group(['prefix' => 'action'], function () {
        Route::get('/', 'api\ActionsController@index')
            ->middleware('permission.auth:actions_view');

        Route::post('/', 'api\ActionsController@save')
            ->middleware('permission.auth:actions_create,actions_edit');

        Route::get('{id}', 'api\ActionsController@get')
            ->where('id', '[0-9]+');

        Route::delete('{id}', 'api\ActionsController@destroy')
            ->where('id', '[0-9]+')
            ->middleware('permission.auth:actions_delete');

        Route::get('list', 'api\ActionsController@listIndex');
    });

    //-------------------------------------------------------------------------
    // Modulo solicitudes
    //-------------------------------------------------------------------------
    Route::group(['prefix' => 'request'], function () {
        Route::get('/', 'api\RequestController@index')
            ->middleware('permission.auth:request_view');

        Route::post('/', 'api\RequestController@save')
            ->middleware('permission.auth:request_create,request_edit');

        Route::get('/pages', 'api\RequestController@pages')
            ->middleware('permission.auth:request_create,request_edit');

        Route::get('{id}', 'api\RequestController@get')
            ->where('id', '[0-9]+');

            Route::get('/approve/{id}', 'api\RequestController@approve')
            ->where('id', '[0-9]+')
            ->middleware('permission.auth:request_gestor');

            Route::get('/deny/{id}', 'api\RequestController@deny')
            ->where('id', '[0-9]+')
            ->middleware('permission.auth:request_gestor');

            Route::get('/comments/{id}', 'api\RequestController@comments')
            ->where('id', '[0-9]+')
            ->middleware('permission.auth:request_comment');

            Route::post('/saveComment', 'api\RequestController@saveComment')
            ->middleware('permission.auth:request_comment');



        // Route::delete('{id}', 'api\ActionsController@destroy')
        //     ->where('id', '[0-9]+')
        //     ->middleware('permission.auth:actions_delete');

        // Route::get('list', 'api\ActionsController@listIndex');
    });

    
    //-------------------------------------------------------------------------
    // Modulo páginas
    //-------------------------------------------------------------------------
    Route::group(['prefix' => 'pages'], function () {
        Route::get('list', 'PageController@listIndex')
        ->middleware('permission.auth:pages_view');
    });




    //-------------------------------------------------------------------------
    // Modulo páginas
    //-------------------------------------------------------------------------
    Route::group(['prefix' => 'models'], function () {
        Route::post('list', 'ModelController@listLookup');
    });

    // Modulo sedes
    //-------------------------------------------------------------------------
    Route::group(['prefix' => 'sedes'], function () {
        Route::get('list', 'SedesController@listIndex');
    });

    //-------------------------------------------------------------------------
    // Modulo studios
    //-------------------------------------------------------------------------
    Route::group(['prefix' => 'studios'], function () {
        Route::get('list', 'StudioController@listIndex');
    });

    

});

Route::group(array('middleware' => ['cors']), function () {

    //-------------------------------------------------------------------------
    // Modulo páginas
    //-------------------------------------------------------------------------
    Route::group(['prefix' => 'pages'], function () {
        Route::get('categorybytype/{category}', 'CategoriesController@categoriesByType');
        Route::post('page', 'PageController@store');
        Route::post('editpage', 'PageController@update');
    }); 

    //-------------------------------------------------------------------------
    // Modulo Estudios
    //-------------------------------------------------------------------------
    Route::group(['prefix' => 'studios'], function () {
        Route::get('studio', 'StudioController@index');
        Route::post('studio', 'StudioController@store');
        Route::post('editstudio', 'StudioController@update');
        Route::get('deletestudio/{studio}', 'StudioController@destroy');
    }); 

    //-------------------------------------------------------------------------
    // Modulo Ubicación
    //-------------------------------------------------------------------------
    Route::group(['prefix' => 'locations'], function () {
        Route::get('locationbycategoria/{category}', 'LocationController@LocationByCategoria');
        Route::post('location', 'LocationController@store');
        Route::post('editlocation', 'LocationController@update');
    });  

    //-------------------------------------------------------------------------
    // Modulo Sedes
    //-------------------------------------------------------------------------
    Route::group(['prefix' => 'sedes'], function () {
        Route::get('sede', 'SedesController@index');
        Route::post('sede', 'SedesController@store');
        Route::post('editsede', 'SedesController@update');
    });  
    
    //-------------------------------------------------------------------------
    // Modulo Informacion Basica
    //-------------------------------------------------------------------------
    Route::group(['prefix' => 'informacion'], function () {
        Route::get('basicinfo/{categoria}', 'BasicInformationController@basicinformationbyc');
        Route::post('basicinfo', 'BasicInformationController@store');
        Route::post('editbasicinfo', 'BasicInformationController@update');
    });  

    //-------------------------------------------------------------------------
    // Modulo Modelos
    //-------------------------------------------------------------------------
    Route::group(['prefix' => 'models'], function () {
        Route::get('modelos', 'ModelController@index');
        Route::post('findmodelos', 'ModelController@show');
        Route::post('modelos', 'ModelController@store');
        Route::post('editmodelo', 'ModelController@update');
        Route::get('deletemodelo/{modelo}', 'ModelController@destroy');
        Route::post('uploadsale', 'SalesController@import');
    }); 
 
    //-------------------------------------------------------------------------
    // Modulo Nicks
    //-------------------------------------------------------------------------
    Route::group(['prefix' => 'nicks'], function () {
        Route::get('nick/{id}', 'NickController@show');
        Route::post('nick', 'NickController@store');
        Route::post('editnick', 'NickController@update');
        Route::get('nickdelete/{id}', 'NickController@destroy');
    }); 

    //-------------------------------------------------------------------------
    // Modulo Parejas
    //-------------------------------------------------------------------------
    Route::group(['prefix' => 'parejas'], function () {
        Route::get('parmodelos', 'ModelsCoupleController@index');
        Route::post('parmodelos', 'ModelsCoupleController@store');
        Route::post('editparmodelo', 'ModelsCoupleController@update');
        Route::get('deleteparmodelo/{modelsCouple}', 'ModelsCoupleController@destroy');
    }); 
 
    //-------------------------------------------------------------------------
    // Modulo Monitores
    //-------------------------------------------------------------------------
    Route::group(['prefix' => 'monitores'], function () {
        Route::get('monitor', 'MonitorController@index');
        Route::post('monitor', 'MonitorController@store');
        Route::post('editmonitor', 'MonitorController@update');
        Route::get('deletemonitor/{monitor}', 'MonitorController@destroy');
        Route::get('modelosMonitor/{monitorid}', 'ModelsMonitorController@getModelosMonitor');
        Route::post('modelosMonitor', 'ModelsMonitorController@store');
        Route::get('modelosMonitorDelete/{monitor}', 'ModelsMonitorController@destroy');
    }); 

    //-------------------------------------------------------------------------
    // Modulo Nicks Parjas 
    //-------------------------------------------------------------------------
    Route::group(['prefix' => 'nicks_parejas'], function () {
        Route::get('nickParejas/{id}', 'NickController@showCoupleNick');
        Route::post('couplenick', 'NickController@addCoupleNick');
        Route::post('editcouplenick', 'NickController@updateCoupleNick');
    }); 
    
    //-------------------------------------------------------------------------
    // Modulo Reglas 
    //-------------------------------------------------------------------------
    Route::group(['prefix' => 'reglas'], function () {
        Route::get('facturationrule', 'FacturationRuleController@index');
        Route::post('facturationrule', 'FacturationRuleController@store');
        Route::post('facturationruleEdit', 'FacturationRuleController@update');
    });  
    
    //-------------------------------------------------------------------------
    // Modulo Ventas
    //-------------------------------------------------------------------------
    Route::group(['prefix' => 'ventas'], function () {
        Route::get('basicInfo', 'SalesController@basicInfo');
        Route::get('basicinfoliquidar', 'SalesConsolidateController@basicInfo');
        Route::post('nicksventa', 'SalesController@getNicksInfo');
        Route::post('savesnicksventa', 'SalesController@saveSalesManually');        
        Route::post('liquidar', 'SalesConsolidateController@LiquidarModelos');
    });
    
    //-------------------------------------------------------------------------
    // Modulo Descuentos 
    //-------------------------------------------------------------------------
    Route::group(['prefix' => 'descuentos'], function () {
        Route::get('modeldiscount', 'ModelsDiscountController@index');
        Route::post('modeldiscount', 'ModelsDiscountController@store');
        Route::post('editmodeldiscount', 'ModelsDiscountController@update');
        Route::post('getmodeldiscount', 'ModelsDiscountController@show');
        Route::get('deletemodeldiscount/{models_discount}', 'ModelsDiscountController@destroy');
    });
    

    
 
    

    

    //-------------------------------------------------------------------------
    // Modulo Descuentos Modelos
    //-------------------------------------------------------------------------
    // Route::group(['prefix' => 'modelsdiscount'], function () {
    //     Route::get('basicInfo', 'SalesController@basicInfo');
        
    // });
});
