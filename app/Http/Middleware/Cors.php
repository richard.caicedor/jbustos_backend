<?php

namespace App\Http\Middleware;

use Closure;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Http\Middleware\BaseMiddleware;

class Cors extends BaseMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    public function handle($request, Closure $next)
    {
        // try {
        //     $token = $request->header('Authorization');
        //     $request['token'] = $token;
        //     JWTAuth::setToken($token);
        //     $user = JWTAuth::parseToken()->toUser();
        //     session(['user' => $user["nick"]]);
        //     $routeArray = app('request')->route()->getAction();
        //     $controllerAction = class_basename($routeArray['controller']);
        // } catch (JWTException $e) {
        //     if ($e instanceof TokenExpiredException) {
        //         return response()->json([
        //             'error' => 'token_expired',
        //             'code' => $e->getStatusCode(),
        //         ], $e->getStatusCode());
        //     } else if ($e instanceof TokenInvalidException) {
        //         return response()->json([
        //             'error' => "token_invalid",
        //             'code' => $e->getStatusCode(),
        //         ], $e->getStatusCode());
        //     } else {
        //         return response()->json([
        //             'error' => 'Token is required',
        //             'code' => $e->getStatusCode(),

        //         ], $e->getStatusCode());
        //     }
        // }

        return $next($request);
    }
}
