<?php

namespace App\Http\Middleware;

use App\Helpers\Fns;
use Closure;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;

class UserAuthMiddleware
{
    public function handle($request, Closure $next)
    {
        try {
            JWTAuth::parseToken()->authenticate();
        } catch (TokenExpiredException $ex) {
            return Fns::errorResponse('token_expired', null, 401);
        }

        return $next($request);
    }
}
