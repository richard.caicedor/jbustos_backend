<?php

namespace App\Http\Middleware;

use App\Helpers\Fns;
use App\Http\Controllers\api\UsersController;
use Closure;

class PermissionAuthMiddleware
{
    public function handle($request, Closure $next, ...$permissions)
    {
        if (UsersController::hasPermissions($permissions)) {
            return $next($request);
        } else {
            return Fns::errorResponse('forbidden', null, 403);
        }
    }
}
