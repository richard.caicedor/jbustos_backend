<?php

namespace App\Http\Controllers\api;

use App\Helpers\Pager;
use App\Http\Controllers\Controller;
use App\RolActions;
use App\Roles;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RolesController extends Controller
{
    /**
     * Mostrar listado de roles
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $pager = new Pager($request);

        $query = DB::table('roles AS r');

        if ($pager->hasFilter()) {
            $query->where('r.name', 'LIKE', $pager->getLikeFilter());
        }

        $countQuery = clone $query;
        $count = $countQuery->count();

        if ($pager->hasOrder()) {
            $query->orderBy($pager->orderBy, $pager->order);
        } else {
            $query->orderBy('r.id', 'desc');
        }

        $data = $query
            ->select('r.id', 'r.name')
            ->skip($pager->skip)
            ->take($pager->size)
            ->get();

        return response()->json([
            'roles' => $data,
            'count' => $count,
        ]);
    }

    /**
     * Guardar rol
     *
     * @return \Illuminate\Http\Response
     */
    public function save(Request $request)
    {
        $this->validate($request, [
            'id' => 'nullable|integer',
            'name' => 'required|max:50',
        ]);

        DB::beginTransaction();
        try {
            if ($request->id) {
                $role = Roles::find($request->id);
            } else {
                $role = new Roles();
            }

            $role->name = $request->name;
            $role->status_id = 1;
            $role->save();

            DB::commit();
            return response()->json([
                'id' => $role->id,
            ]);
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }

    /**
     * Obtener rol
     *
     * @return \Illuminate\Http\Response
     */
    public function get($id)
    {
        $rol = DB::table('roles AS r')
            ->select('r.id', 'r.name')
            ->where('r.id', $id)
            ->firstOrFail();

        return response()->json([
            'rol' => $rol,
        ]);
    }

    /**
     * Eliminar rol
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Roles::find($id)->delete();

        return response()->json([
            'id' => $id,
        ]);
    }

    public function listIndex()
    {
        $data = DB::table('roles AS r')
            ->select('r.id', 'r.name')
            ->orderBy('r.name')
            ->get();

        return response()->json([
            'roles' => $data,
        ]);
    }

    public function actions($id)
    {
        $actions = DB::table('actions AS a')
            ->leftJoin('rol_actions AS ra', function ($join) use ($id) {
                $join->on('ra.action_id', 'a.id')->where('ra.rol_id', $id);
            })
            ->select('a.id', 'a.name', 'a.description', DB::raw("IF(ra.id IS NULL, 0, 1) AS checked"))
            ->get();

        return response()->json([
            'actions' => $actions,
        ]);
    }

    public function saveActions(Request $request, $id)
    {
        $this->validate($request, [
            'added' => 'array',
            'added.*' => 'integer',
            'removed' => 'array',
            'removed.*' => 'integer',
        ]);

        DB::beginTransaction();
        try {

            foreach ($request->added as $key => $value) {
                $rolAction = new RolActions();
                $rolAction->rol_id = $id;
                $rolAction->action_id = $value;
                $rolAction->save();
            }

            if (count($request->removed)) {
                RolActions::where('rol_id', $id)
                    ->whereIn('action_id', $request->removed)
                    ->delete();
            }

            DB::commit();

            return response()->json([
                'id' => $id,
            ]);
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }
}
