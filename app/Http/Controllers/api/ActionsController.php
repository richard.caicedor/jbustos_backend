<?php

namespace App\Http\Controllers\api;

use App\Action;
use App\Helpers\Pager;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ActionsController extends Controller
{
    /**
     * Mostrar listado de acciones
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $pager = new Pager($request);

        $query = DB::table('actions AS a');

        if ($pager->hasFilter()) {
            $query->where('a.name', 'LIKE', $pager->getLikeFilter())
                ->orWhere('a.description', 'LIKE', $pager->getLikeFilter());
        }

        $countQuery = clone $query;
        $count = $countQuery->count();

        if ($pager->hasOrder()) {
            $query->orderBy($pager->orderBy, $pager->order);
        } else {
            $query->orderBy('a.id', 'desc');
        }

        $data = $query
            ->select('a.id', 'a.name', 'a.description')
            ->skip($pager->skip)
            ->take($pager->size)
            ->get();

        return response()->json([
            'actions' => $data,
            'count' => $count,
        ]);
    }

    /**
     * Guardar acción
     *
     * @return \Illuminate\Http\Response
     */
    public function save(Request $request)
    {
        $this->validate($request, [
            'id' => 'nullable|integer',
            'name' => 'required|max:50',
            'description' => 'required|max:50',
        ]);

        DB::beginTransaction();
        try {
            if ($request->id) {
                $action = Action::find($request->id);
            } else {
                $action = new Action();
            }

            $action->name = $request->name;
            $action->description = $request->description;
            $action->path = '';
            $action->status_id = 1;
            $action->save();

            DB::commit();
            return response()->json([
                'id' => $action->id,
            ]);
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }

    /**
     * Obtener accion
     *
     * @return \Illuminate\Http\Response
     */
    public function get($id)
    {
        $action = DB::table('actions AS a')
            ->select('a.id', 'a.name', 'a.description')
            ->where('a.id', $id)
            ->firstOrFail();

        return response()->json([
            'action' => $action,
        ]);
    }

    /**
     * Eliminar accion
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Action::find($id)->delete();

        return response()->json([
            'id' => $id,
        ]);
    }

    public function listIndex()
    {
        $data = DB::table('actions AS a')
            ->select('a.id', 'a.name', 'a.description')
            ->orderBy('a.name')
            ->get();

        return response()->json([
            'actions' => $data,
        ]);
    }
}
