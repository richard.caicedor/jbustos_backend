<?php

namespace App\Http\Controllers\api;

use App\Helpers\Pager;
use App\Http\Controllers\Controller;
use App\Menu;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MenuController extends Controller
{

    /**
     * Mostrar listado de menú
     *
     * @return \Illuminate\Http\Response
     */
    public function tree()
    {
        $menus = DB::table('menu AS m')
            ->leftJoin('actions AS p', 'm.action_id', 'p.id')
            ->select('m.id', 'm.name', 'm.url', 'm.icon', 'm.order', 'm.menu_id_parent', 'm.action_id', 'p.name AS action_name')
            ->orderBy('m.menu_id_parent', 'asc')
            ->orderBy('m.order', 'asc')
            ->get();

        $fn = function ($currentMenus, $fnParam, $parent) use ($menus) {
            if ($parent == null) {
                $parent = (object) [];
                $parent->children = [];
            }

            foreach ($currentMenus as $key => $menu) {
                $node = (object) [];
                $node->id = $menu->id;
                $node->name = $menu->name;
                $node->order = $menu->order;

                if ($menu->icon != null) {
                    $node->icon = $menu->icon;
                }

                if ($menu->url == null) {
                    $node->children = [];
                    $children = $menus->where('menu_id_parent', $menu->id);

                    $fnParam($children, $fnParam, $node);
                } else {
                    $node->url = $menu->url;
                    $node->action_id = $menu->action_id;
                    $node->action_name = $menu->action_name;
                }

                array_push($parent->children, $node);
            }
            return $parent;
        };

        $rootMenus = $menus->where('menu_id_parent', null);
        $mainTree = $fn($rootMenus, $fn, null);

        return response()->json([
            'menu' => $mainTree->children,
        ]);
    }

    public function listIndex()
    {
        $data = DB::table('menu AS m')
            ->select('m.id', 'm.name', 'm.url')
            ->orderBy('m.name')
            ->get();

        return response()->json([
            'menus' => $data,
        ]);
    }

    /**
     * listado pagindor de menus
     */
    public function index(Request $request)
    {
        $pager = new Pager($request);

        $query = DB::table('menu AS m')
            ->leftJoin('menu AS mp', 'mp.id', 'm.menu_id_parent');

        if ($pager->hasFilter()) {
            $query->where('m.name', 'LIKE', $pager->getLikeFilter())
                ->orWhere('m.url', 'LIKE', $pager->getLikeFilter())
                ->orWhere('mp.name', 'LIKE', $pager->getLikeFilter());
        }

        $countQuery = clone $query;
        $count = $countQuery->count();

        if ($pager->hasOrder()) {
            $query->orderBy($pager->orderBy, $pager->order);
        } else {
            $query->orderBy('m.id', 'desc');
        }

        $data = $query
            ->select('m.id', 'm.name', 'm.url', 'mp.name AS parent')
            ->skip($pager->skip)
            ->take($pager->size)
            ->get();

        return response()->json([
            'menus' => $data,
            'count' => $count,
        ]);
    }

    /**
     * Guardar menu
     */
    public function save(Request $request)
    {
        $this->validate($request, [
            'id' => 'nullable|integer',
            'name' => 'required|max:45',
            'url' => 'max:100',
            'icon' => 'max:45',
            'order' => 'required|integer',
            'menu_id_parent' => 'nullable|integer',
            'action_id' => 'nullable|integer',
        ]);

        DB::beginTransaction();
        try {
            if ($request->id) {
                $menu = Menu::find($request->id);
            } else {
                $menu = new Menu();
            }

            $menu->name = $request->name;
            $menu->url = $request->url;
            $menu->icon = $request->icon;
            $menu->order = $request->order;
            $menu->menu_id_parent = $request->menu_id_parent;
            $menu->action_id = $request->action_id;
            $menu->save();

            DB::commit();
            return response()->json([
                'id' => $menu->id,
            ]);
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }

    /**
     * Obtener menu
     */
    public function get($id)
    {
        $menu = DB::table('menu AS m')
            ->select('m.id', 'm.name', 'm.url', 'm.icon', 'm.order', 'm.menu_id_parent', 'm.action_id')
            ->where('m.id', $id)
            ->firstOrFail();

        return response()->json([
            'menu' => $menu,
        ]);
    }

    /**
     * Eliminar menu
     */
    public function destroy($id)
    {
        Menu::find($id)->delete();

        return response()->json([
            'id' => $id,
        ]);
    }

}
