<?php

namespace App\Http\Controllers\api;

use App\Helpers\Fns;
use App\Helpers\Pager;
use App\Http\Controllers\Controller;
use App\User;
use App\UserRole;
use Auth;
use Carbon\Carbon;
use Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Tymon\JWTAuth\Facades\JWTAuth;

class UsersController extends Controller
{

    public function login(Request $request)
    {
        $this->validate($request, [
            'nick' => 'required',
            'password' => 'required',
        ]);

        $user = DB::table('users AS u')
            ->select('u.id', 'u.name', 'u.nick', 'u.password')
            ->where('u.nick', $request->nick)
            ->first();

        if (!$user) {
            return Fns::errorResponse('login_failed');
        } else if (!Hash::check($request->password, $user->password)) {
            return Fns::errorResponse('login_failed');
        }

        $token = $this->generateAuthToken($user->id, Carbon::now()->addDays(1)->timestamp);

        unset($user->password);

        return response()->json([
            'user' => $user,
            'actions' => $this->actions($user),
            'token' => $token->token,
            'timestamp' => $token->timestamp,
        ]);
    }

    public function logout()
    {
        JWTAuth::invalidate();
        Auth::logout();
    }

    /**
     * Generate auth token
     *
     * @param integer $id
     * @param timestamp $expTimestamp
     * @return string Auth JWT Token
     */
    private function generateAuthToken($id, $expTimestamp)
    {
        $authPerson = User::findOrFail($id);
        $authPerson->customClaims = [
            'exp' => $expTimestamp,
            'iss' => config('app.url'),
        ];

        return (object) [
            'token' => JWTAuth::fromUser($authPerson),
            'timestamp' => $expTimestamp - Carbon::now()->timestamp,
        ];
    }

    public function current()
    {
        $user = Auth::user();
        $cloned = (object) [
            'id' => $user->id,
            'name' => $user->name,
            'nick' => $user->nick,
        ];

        return response()->json([
            'user' => $cloned,
            'actions' => $this->actions($cloned),
        ]);
    }

    private function actions($user)
    {
        $userRolesId = DB::table('user_roles')
            ->where('user_id', $user->id)
            ->select('role_id')
            ->get()
            ->pluck('role_id')
            ->toArray();

        $actions = DB::table('rol_actions AS ra')
            ->join('actions AS a', 'a.id', 'ra.action_id')
            ->whereIn('ra.rol_id', $userRolesId)
            ->distinct()
            ->select('a.name')
            ->get()
            ->pluck('name')
            ->toArray();

        return $actions;
    }

    /**
     * Verifica que la persona tenga alguno de los permisos especificados
     * @param array $permissions nombres de los permisos
     * @param int $userId Id de las persona, si no se suministra se toma el usuario de la sesión actual
     * @return boolean
     */
    public static function hasPermissions($permissions, $userId = null)
    {
        $userId = $userId ?? Auth::id();

        $userRolesId = DB::table('user_roles')
            ->where('user_id', $userId)
            ->select('role_id')
            ->get()
            ->pluck('role_id')
            ->toArray();

        return DB::table('rol_actions AS ra')
            ->join('actions AS a', 'a.id', '=', 'ra.action_id')
            ->whereIn('ra.rol_id', $userRolesId)
            ->whereIn('a.name', $permissions)
            ->exists();
    }

    public function changePassword(Request $request)
    {
        $this->validate($request, [
            'current' => 'required',
            'new' => 'required|min:8',
        ]);

        DB::beginTransaction();
        try {
            $user = User::findOrFail(Auth::id());

            if (!Hash::check($request->current, $user->password)) {
                return Fns::errorResponse('invalid_current_password');
            }

            $user->password = Hash::make($request->new);
            $user->save();

            DB::commit();
            return response()->json([
                'id' => $user->id,
            ]);
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }

    /**
     * listado pagindor de usuarios
     */
    public function index(Request $request)
    {
        $pager = new Pager($request);

        $query = DB::table('users AS u')
            ->leftJoin('studios AS s', 's.id', 'u.studio_id')
            ->leftJoin('sedes AS se', 'se.id', 'u.sede_id');

        if ($pager->hasFilter()) {
            $query->where('u.name', 'LIKE', $pager->getLikeFilter())
                ->orWhere('u.nick', 'LIKE', $pager->getLikeFilter())
                ->orWhere('s.name', 'LIKE', $pager->getLikeFilter())
                ->orWhere('se.name', 'LIKE', $pager->getLikeFilter());
        }

        $countQuery = clone $query;
        $count = $countQuery->count();

        if ($pager->hasOrder()) {
            $query->orderBy($pager->orderBy, $pager->order);
        } else {
            $query->orderBy('u.id', 'desc');
        }

        $data = $query
            ->select('u.id', 'u.name', 'u.nick', 'u.email', 'se.name AS sede_name', 's.name AS studio_name')
            ->skip($pager->skip)
            ->take($pager->size)
            ->get();

        return response()->json([
            'users' => $data,
            'count' => $count,
        ]);
    }

    /**
     * Guardar usuario
     */
    public function save(Request $request)
    {
        $this->validate($request, [
            'id' => 'nullable|integer',
            'name' => 'required|max:255',
            'nick' => 'required|max:255',
            'email' => 'required|max:255',
            'sede_id' => 'nullable|integer',
            'studio_id' => 'nullable|integer',
            'roles' => 'present',
            'roles.added' => 'array',
            'roles.added.*' => 'integer',
            'roles.removed' => 'array',
            'roles.removed.*' => 'integer',
        ]);

        if (!$request->id || $request->password) {
            $this->validate($request, [
                'password' => 'required|min:8',
            ]);
        }

        DB::beginTransaction();
        try {
            if ($request->id) {
                $user = User::find($request->id);
            } else {
                $user = new User();
            }

            $user->name = $request->name;
            $user->nick = $request->nick;
            $user->email = $request->email;
            $user->sede_id = $request->sede_id;
            $user->studio_id = $request->studio_id;

            if ($request->password) {
                $user->password = Hash::make($request->password);
            }

            $user->save();

            foreach ($request->roles['added'] as $roleId) {
                UserRole::firstOrCreate([
                    'user_id' => $user->id,
                    'role_id' => $roleId,
                ]);
            }

            if (count($request->roles['removed'])) {
                UserRole::where('user_id', $user->id)
                    ->whereIn('role_id', $request->roles['removed'])
                    ->delete();
            }

            DB::commit();
            return response()->json([
                'id' => $user->id,
            ]);
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }

    /**
     * Obtener usuario
     */
    public function get($id)
    {
        $user = DB::table('users AS u')
            ->select('u.id', 'u.name', 'u.nick', 'u.email', 'u.sede_id', 'u.studio_id')
            ->where('u.id', $id)
            ->firstOrFail();

        $user->roles = DB::table('user_roles AS ur')
            ->select('ur.role_id')
            ->where('ur.user_id', $id)
            ->pluck('role_id')
            ->toArray();

        return response()->json([
            'user' => $user,
        ]);
    }

    /**
     * Eliminar usuario
     */
    public function destroy($id)
    {
        User::find($id)->delete();

        return response()->json([
            'id' => $id,
        ]);
    }

}
