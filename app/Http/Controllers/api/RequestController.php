<?php

namespace App\Http\Controllers\api;

use App\Helpers\Fns;
use App\Helpers\Pager;
use App\Http\Controllers\api\UsersController;
use App\Http\Controllers\Controller;
use App\Request as RQ;
use App\RequestComment;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class RequestController extends Controller
{
    /**
     * Mostrar listado de roles
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $pager = new Pager($request);

        $query = DB::table('requests AS r')
            ->leftJoin('models AS m', 'r.model_id', '=', 'm.id')
            ->leftJoin('models_couples AS mc', 'r.mode_couple_id', '=', 'mc.id')
            ->leftJoin('models AS mc1', 'mc1.id', '=', 'mc.model_id')
            ->leftJoin('models AS mc2', 'mc2.id', '=', 'mc.couple_id')
            ->join('pages AS p', 'r.page_id', '=', 'p.id')
            ->join('statuses AS s', 'r.status_id', '=', 's.id');

        if (!(UsersController::hasPermissions(["request_gestor"], Auth::id()))) {
            $query->where('r.created_by', '=', Auth::id());
        }

        if ($pager->hasFilter()) {
            $query->where('m.name', 'LIKE', $pager->getLikeFilter())
                ->orWhere('p.name', 'LIKE', $pager->getLikeFilter());
        }

        $countQuery = clone $query;
        $count = $countQuery->count();

        if ($pager->hasOrder()) {
            $query->orderBy($pager->orderBy, $pager->order);
        } else {
            $query->orderBy('r.id', 'desc');
        }

        $data = $query
            ->select('r.id', DB::raw("IF(m.id IS NULL, CONCAT(mc1.name, ' | ', mc2.name), m.name) AS model"), 'p.name as page', 's.description as status', 'r.status_id')
            ->get();

        return response()->json([
            'solicitudes' => $data,
            'count' => $count,
        ]);
    }

    /**
     * Guardar rol
     *
     * @return \Illuminate\Http\Response
     */
    public function save(Request $request)
    {
        $this->validate($request, [
            'id' => 'nullable|integer',
            'model_id' => 'required',
        ]);

        DB::beginTransaction();
        try {
            if ($request->id) {
                $rq = RQ::find($request->id);
            } else {
                $rq = new RQ();
                $rq->page_id = $request->pages[0];
                $rq->status_id = 6;
            }

            if ($request->type == 1) {
                $rq->model_id = $request->model_id;
                $rq->mode_couple_id = null;
            } else {
                $rq->mode_couple_id = $request->model_id;
                $rq->model_id = null;
            }

            $rq->urlDropBox = $request->urlDropBox;
            $rq->create_profile = $request->create_profile;
            $rq->old_profile = $request->old_profile;
            $rq->nicks_old_profile = $request->nicks_old_profile;
            $rq->creator_old_profile = $request->creator_old_profile;
            $rq->other = $request->other;
            $rq->specifications = $request->specifications;
            $rq->social_media = $request->social_media;
            $rq->social_media_profile = $request->social_media_profile;
            $rq->about_me = $request->about_me;
            $rq->tip_menu = $request->tip_menu;
            $rq->rules = $request->rules;
            $rq->comments = $request->comments;
            $rq->nicks_suggest = $request->nicks_suggest;
            $rq->created_by = Auth::id();

            $id = 0;

            if (!$request->id) {
                foreach ($request->pages as $page) {
                    $rq2 = $rq->replicate();
                    $rq2->page_id = $page;
                    $rq2->save();

                    $data = (object) [];
                    $data->id = $rq2->id;
                    $data->comment = "Solicitud Registrada.";
                    $data->status_id_comment = $rq2->status_id;

                    $this->saveOnlyComment($data);

                    //DB::table('requests')->insert($rq->ToArray());
                    $id = $id + 1;
                }
            } else {
                $rq->save();
                $id = $rq->id;
            }
            DB::commit();

            return response()->json([
                'id' => $id,
            ]);
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }

    /**
     * Obtener rol
     *
     * @return \Illuminate\Http\Response
     */
    public function get($id)
    {
        $request = DB::table('requests AS r')
            ->select('r.*'
            )
            ->where('r.id', $id)
            ->firstOrFail();

        $request->model_id = $request->model_id ? ($request->model_id . '-1') : ($request->mode_couple_id . '-2');

        return response()->json([
            'request' => $request,
        ]);
    }

    public function approve($id)
    {
        try {
            DB::beginTransaction();
            $rq = RQ::find($id);

            if ($rq->status_id === 10 || $rq->status_id === 11) {
                return Fns::errorResponse('request_already_procesed');
            }

            $rq->status_id = 10;
            $rq->save();

            DB::commit();

            $data = (object) [];
            $data->id = $id;
            $data->comment = "Solicitud Aprobada.";
            $data->status_id_comment = $rq->status_id;

            $this->saveOnlyComment($data);

            return response()->json([
                'request' => $rq->id,
            ]);
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }

    public function deny($id)
    {
        try {
            DB::beginTransaction();
            $rq = RQ::find($id);

            if ($rq->status_id === 10 || $rq->status_id === 11) {
                return Fns::errorResponse('request_already_procesed');
            }

            $rq->status_id = 11;
            $rq->save();

            DB::commit();

            $data = (object) [];
            $data->id = $id;
            $data->comment = "Solicitud Rechazada.";
            $data->status_id_comment = $rq->status_id;

            $this->saveOnlyComment($data);

            return response()->json([
                'request' => $rq->id,
            ]);
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }

    public function comments($id)
    {
        $request = DB::table('request_comments AS rc')
            ->leftJoin('users AS u', 'u.id', 'rc.created_by')
            ->leftJoin('statuses AS s', 'rc.status_id', '=', 's.id')
            ->select('rc.id', 'rc.comment', 'u.name AS user_name', 'rc.created_at', 's.description as status')
            ->where('rc.request_id', $id)
            ->orderBy('rc.id', 'desc')
            ->get();

        return response()->json([
            'comments' => $request,
        ]);
    }

    public function saveComment(Request $request)
    {
        try {

            $rqcomment = $this->saveOnlyComment($request);

            DB::beginTransaction();

            $rq = RQ::find($rqcomment->request_id);

            if ($rq->status_id === 10 || $rq->status_id === 11) {
                return Fns::errorResponse('request_already_procesed');
            }

            $rq->status_id = $request->status_id_comment;
            $rq->save();

            DB::commit();

        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }

        return response()->json([
            'id' => $rqcomment->id,
            'created_at' => $rqcomment->created_at,
        ]);
    }

    public function saveOnlyComment($request)
    {
        $rqcomment = new RequestComment();

        try {
            DB::beginTransaction();

            $rqcomment->request_id = $request->id;
            $rqcomment->comment = $request->comment;
            $rqcomment->created_by = Auth::id();
            $rqcomment->status_id = $request->status_id_comment;

            $rqcomment->save();
            DB::commit();

        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }

        return $rqcomment;
    }

    /**
     * Eliminar rol
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Roles::find($id)->delete();

        return response()->json([
            'id' => $id,
        ]);
    }

    // public function pages()
    // {
    //     $pages = DB::table('pages AS p')
    //         ->leftJoin('requests AS r', function ($join){
    //             $join->on('r.page_id', 'p.id');//->where('ra.rol_id', $id);
    //         })
    //         ->select('p.id', 'p.name', 'p.url', DB::raw("IF(r.id IS NULL, 0, 1) AS checked"))
    //         ->get();

    //     return response()->json([
    //         'pages' => $pages,
    //     ]);
    // }

}
