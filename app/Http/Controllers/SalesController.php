<?php

namespace App\Http\Controllers;

use App\Sales;
use App\Page;
use App\Sedes;
use App\Nick;
use App\Imports\SalesImport;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\DB;

class SalesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Sales::with('nickinfo')->get();
    }

    public function basicInfo()
    {
        $sedes = Sedes::get();
        $pages = Page::get();
        
        return response()->json([
            'sedes' => $sedes,
            'paginas' => $pages  
        ]);
    }

    public function getNicksInfo(Request $request)
    {
        $requestInfo = json_decode($request->getContent(), true);
        
        $nicks = DB::table('nicks AS n')
                   ->leftJoin('models AS m',function ($join) {
                        $join->on('m.id', '=', 'n.ownable_id')
                            ->where("n.ownable_type","App\Modelo");
                   })                   
            ->select('n.id as model_id', 
                     'm.identification as identificacion', 
                     DB::raw("concat(m.name,' ',m.lastname) as nombre, '' as ventas"), 
                     'n.nickname as nick')
            ->where('m.sede_id', $requestInfo['sede_id'])
            ->where('n.page_id', $requestInfo['page_id'])       
            ->orderBy('n.nickname', 'asc')
            ->orderBy('m.name', 'asc')->get();

        return $nicks;
    }

    public function saveSalesManually(Request $request)
    {
        $requestInfo = json_decode($request->getContent(), true);
        $cant = count($requestInfo);
        $filters = $requestInfo[$cant-1];
        for ($i=0; $i <  $cant-1; $i++) {
            if($requestInfo[$i]['ventas'] > 0){

                $nick = Nick::where('id', $requestInfo[$i]['model_id'])
                          ->first();
                $modelInfo = $nick->ownable;
                $page = Page::find($nick->page_id);
                $type = $page->category_id;
    
                $sales = new Sales;
    
                if($type == 13){
                    $sales->tokens = $requestInfo[$i]['ventas'];
                    $sales->dollars = $requestInfo[$i]['ventas'] * 0.05;
                }else if($type == 14){
                    $sales->dollars = $requestInfo[$i]['ventas'];
                }
    
                $sales->nick_id = $nick->id;
                $sales->page_id = $nick->page_id;
    
                if($nick->ownable_type == "App\Modelo"){
                    $sales->sede_id = $modelInfo->sede_id;
                    $sales->model_id = $modelInfo->id;
                }else{
                    $sales->sede_id = $modelInfo->modeloinfo->sede_id;
                    $sales->modelcouple_id = $modelInfo->id;
                }
                $sales->sales_date = $filters['fecha_inicio'];
                $sales->sales_enddate = $filters['fecha_fin'];
    
                $sales->save();
            }

        }
        
        return response()->json([
            'status' => 'Ok',
            'message' => 'Ventas almacenadas',
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    public function import(Request $request) 
    {
        $file = $request->file('file');
        Excel::import(new SalesImport($request->page_id,$request->fecha_inicio,$request->fecha_fin), $file); 
        
       return $request->page_id;  
    }

    public function liquidar(Request $request) 
    {
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Sales  $sales
     * @return \Illuminate\Http\Response
     */
    public function show(Sales $sales)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Sales  $sales
     * @return \Illuminate\Http\Response
     */
    public function edit(Sales $sales)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Sales  $sales
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Sales $sales)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Sales  $sales
     * @return \Illuminate\Http\Response
     */
    public function destroy(Sales $sales)
    {
        //
    }
}
