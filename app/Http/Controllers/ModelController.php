<?php

namespace App\Http\Controllers;

use App\BasicInformation;
use App\Helpers\Fns;
use App\Location;
use App\Modelo;
use App\Nick;
use App\Page;
use App\Sedes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ModelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $models = Modelo::with("nicks")
            ->leftJoin('bank_accounts', function ($join) {
                $join->on('models.id', '=', 'bank_accounts.model_id')
                    ->where("bank_accounts.status_id", 21);
            })
            ->select("models.*",
                "bank_accounts.account_number as cuenta",
                "bank_accounts.own_name as propietario",
                "bank_accounts.bank_id as banco_id")
            ->get();
        //var_dump($models);
        //die();
        $sedes = Sedes::get();
        $pages = Page::get();
        $eps = BasicInformation::where('category_id', 23)->get();
        $arl = BasicInformation::where('category_id', 24)->get();
        $fp = BasicInformation::where('category_id', 25)->get();
        $cdf = BasicInformation::where('category_id', 29)->get();
        $paises = Location::where('category_id', 15)->get();
        $departamentos = Location::where('category_id', 16)->get();
        $ciudades = Location::where('category_id', 17)->get();
        $nicks = Nick::get();
        $paginas = Page::get();
        $bancos = BasicInformation::where('category_id', 26)->get();

        return response()->json([
            'modelos' => $models,
            'sedes' => $sedes,
            'paginas' => $pages,
            'eps' => $eps,
            'arl' => $arl,
            'fp' => $fp,
            'paises' => $paises,
            'ciudades' => $ciudades,
            'departamentos' => $departamentos,
            'cdf' => $cdf,
            'nicks' => $nicks,
            'paginas' => $paginas,
            'bancos' => $bancos,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request = json_decode($request->getContent(), true);
        $models = new Modelo();

        $models->identification = $request["identification"]; //OK
        $models->name = $request["name"]; //OK
        $models->lastname = $request["lastname"]; //OK
        $models->address = $request["address"]; //OK
        $models->email = $request["email"]; //OK
        $models->cellphone = $request["cellphone"]; //OK
        $models->admission_date = $request["admission_date"]; //OK
        $models->birth_date = $request["birth_date"]; // OK
        $models->sede_id = $request["sede_id"]; //OK
        $models->birth_country = $request["birth_country"];
        $models->birth_city = $request["birth_city"];
        $models->eps_id = $request["eps_id"]; //OK
        $models->afp_id = $request["afp_id"]; //OK
        $models->ccf_id = $request["ccf_id"]; //OK
        $models->fdp_id = $request["fdp_id"];
        $models->country_id = $request["country_id"]; //OK
        $models->city_id = $request["city_id"]; //OK
        $models->status_id = 13;
        $models->category_id = 1;

        $models->save();

        $bankCon = new BankAccountController();
        $bankCon->createAccount($models->id, $request['cuenta'], $request['propietario'], $request['banco_id']);

        return $models;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Modelo  $modelo
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $request = json_decode($request->getContent(), true);
        $models = Modelo::where('status_id', 13);

        if ($request['identification'] != '') {
            $models->where('identification', $request['identification']);
        }

        if ($request['email'] != '') {
            $models->where('email', $request['email']);
        }

        if ($request['nombre'] != '') {
            $models->where(function ($query) {
                $query->where('name', 'like', '%' . $request['nombre'] . '%')
                    ->orWhere('lastname', 'like', '%' . $request['nombre'] . '%');
            });
        }

        if ($request['sede_id'] != '') {
            $models->where('sede_id', $request['sede_id']);
        }

        return $models->get();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Modelo  $modelo
     * @return \Illuminate\Http\Response
     */
    public function edit(Modelo $modelo)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Modelo  $modelo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $request = json_decode($request->getContent(), true);
        $models = Modelo::find($request['id']);

        $models->identification = $request["identification"];
        $models->name = $request["name"];
        $models->lastname = $request["lastname"];
        $models->address = $request["address"];
        $models->email = $request["email"];
        $models->cellphone = $request["cellphone"];
        $models->admission_date = $request["admission_date"];
        $models->birth_date = $request["birth_date"];
        $models->sede_id = $request["sede_id"];
        $models->eps_id = $request["eps_id"];
        $models->afp_id = $request["afp_id"];
        $models->ccf_id = $request["ccf_id"];
        $models->fdp_id = $request["fdp_id"];
        $models->country_id = $request["country_id"];
        $models->city_id = $request["city_id"];
        $models->save();

        $bankCon = new BankAccountController();
        $bankCon->createAccount($models->id, $request['cuenta'], $request['propietario'], $request['banco_id']);

        return $models;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Modelo  $modelo
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $models = Modelo::find($id);
        $models->delete();

        return $models;
    }

    public function listLookup(Request $request)
    {
        $query = DB::table('models AS m')
            ->leftJoin('models_couples as mc1', 'm.id', "=", "mc1.model_id")
            ->leftJoin('models as mc1m', 'mc1m.id', "=", "mc1.couple_id")
            ->leftJoin('models_couples as mc2', 'm.id', "=", "mc2.couple_id")
            ->leftJoin('models as mc2m', 'mc2m.id', "=", "mc2.model_id")
            ->select('m.id', 'm.name', 'mc1.id as couple1_id', 'mc2.id as couple2_id', 'mc1.model_id', 'mc2.couple_id', 'mc2m.name as couple2_name', 'mc1m.name as couple1_name',DB::raw("1 AS type"));

        if ($request->initial) { // 1-2
            $temp = explode('-', $request->initial);

            if ($temp[1] == 1) {
                $data = $query->where('m.id', $temp[0])->firstOrFail();
            } else {
                $data = $query->where('mc1.id', $temp[0])
                    ->orWhere('mc2.id', $temp[0])
                    ->firstOrFail();

                $data->type = 2;
                $data->name = $data->name . ' | ' . ($data->model_id != null ? $data->couple1_name : $data->couple2_name);
            }

            return response()->json([
                'models' => $data,
            ]);

        } else {

            $data = $query->where('m.name', 'LIKE', Fns::getLikeFilter($request->q))
                ->take(50)
                ->orderBy('m.name')
                ->get();

            $parejas = collect([]);

            foreach ($data as $n) {
                if ($n->type === 1 && ($n->model_id != null || $n->couple_id != null)) {
                    $n2 = (object) [];
                    $n2->id = $n->model_id != null ? $n->couple1_id : $n->couple2_id;
                    $n2->type = 2;
                    $n2->name = $n->name . ' | ' . ($n->model_id != null ? $n->couple1_name : $n->couple2_name);
                    $n2->new_id = $n2->id . '-2';

                    $parejas->push($n2);
                }
                $n->new_id = $n->id . '-1';
                $parejas->push($n);
            };

            return response()->json([
                'models' => $parejas,
            ]);
        }
    }
}
