<?php

namespace App\Http\Controllers;

use App\Monitor;
use App\Modelo;
use App\BasicInformation;
use App\Location;
use Illuminate\Http\Request;

class MonitorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $monitor = Monitor::where('status_id',21)->get();
        $models = Modelo::with("nicks")->get();
        $eps = BasicInformation::where('category_id',23)->get();
        $arl = BasicInformation::where('category_id',24)->get();
        $fp = BasicInformation::where('category_id',25)->get();
        $cdf = BasicInformation::where('category_id',29)->get();
        $ciudades = Location::where('category_id',17)->get();

        return response()->json([
            'modelos' => $models,
            'monitores' => $monitor,
            'eps' => $eps,
            'arl' => $arl,
            'fp' => $fp,
            'ciudades' => $ciudades,
            'cdf' => $cdf
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $monitorInfo = json_decode($request->getContent(), true);
        $monitor = new Monitor();
        $monitor->identification    = $monitorInfo['identification'];
        $monitor->name              = $monitorInfo['name'];
        $monitor->lastname          = $monitorInfo['lastname'];
        $monitor->address           = $monitorInfo['address'];
        $monitor->email             = $monitorInfo['email'];
        $monitor->cellphone         = $monitorInfo['cellphone'];
        $monitor->admission_date    = $monitorInfo['admission_date'];
        $monitor->eps_id            = $monitorInfo['eps_id'];
        $monitor->afp_id            = $monitorInfo['afp_id'];
        $monitor->fdp_id            = $monitorInfo['fdp_id'];
        $monitor->ccf_id            = $monitorInfo['ccf_id'];
        $monitor->city_id           = $monitorInfo['city_id'];
        $monitor->category_id       = 1;
        $monitor->status_id         = 21;

        $monitor->save();
        return $monitor;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Monitor  $monitor
     * @return \Illuminate\Http\Response
     */
    public function show(Monitor $monitor)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Monitor  $monitor
     * @return \Illuminate\Http\Response
     */
    public function edit(Monitor $monitor)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Monitor  $monitor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Monitor $monitor)
    {
        $monitorInfo = json_decode($request->getContent(), true);
        $monitor = Monitor::find($monitorInfo['id']);

        $monitor->identification    = $monitorInfo['identification'];
        $monitor->name              = $monitorInfo['name'];
        $monitor->lastname          = $monitorInfo['lastname'];
        $monitor->address           = $monitorInfo['address'];
        $monitor->email             = $monitorInfo['email'];
        $monitor->cellphone         = $monitorInfo['cellphone'];
        $monitor->admission_date    = $monitorInfo['admission_date'];
        $monitor->eps_id            = $monitorInfo['eps_id'];
        $monitor->afp_id            = $monitorInfo['afp_id'];
        $monitor->fdp_id            = $monitorInfo['fdp_id'];
        $monitor->ccf_id            = $monitorInfo['ccf_id'];
        $monitor->city_id           = $monitorInfo['city_id'];

        $monitor->save();
        return $monitor;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Monitor  $monitor
     * @return \Illuminate\Http\Response
     */
    public function destroy($monitor)
    {
        $monitor = Monitor::find($monitor);
        $monitor->delete();

        return response()->json([
            'status'=> "Ok", 
            'message'=> "Registro Eliminado" 
        ]);
    }
}
