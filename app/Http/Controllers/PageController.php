<?php

namespace App\Http\Controllers;

use App\Page;
use App\PagesSede;
use App\Sedes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $paginas = Page::with("categoria")->orderBy("name")->get();

        return $paginas;
    }

    public function paginasSedes()
    {

        $paginas = Page::with("categoria")->orderBy("name")->get();
        $sedes = Sedes::get();
        return response()->json([
            'sedes' => $sedes,
            'paginas' => $paginas,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request = json_decode($request->getContent(), true);
        $txtHttp = 'http://';
        $url = $request['url'];
        $urlogin = $request['url_login'];
        $pos = strpos($url, $txtHttp);
        if ($pos === false && $url != '') {
            $url = $txtHttp . $url;
        }

        $pos = strpos($urlogin, $txtHttp);
        if ($pos === false && $urlogin != '') {
            $urlogin = $txtHttp . $urlogin;
        }

        $paginas = new Page();

        $paginas->name = $request['name'];
        $paginas->url = $url;
        $paginas->url_login = $urlogin;
        $paginas->category_id = $request['category_id'];
        $paginas->column_nick = strtoupper($request['column_nick']);
        $paginas->column_value = strtoupper($request['column_value']);
        $paginas->status_id = 2;

        $paginas->save();

        $sedes = Sedes::get();

        foreach ($sedes as $key => $value) {
            $pageSede = new PagesSede();

            $pageSede->sede_id = $value->id;
            $pageSede->page_id = $paginas->id;
            $pageSede->status_id = 2;

            $pageSede->save();
        }
        return $paginas;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function show(Page $page)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function edit(Page $page)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $request = json_decode($request->getContent(), true);
        $paginas = Page::find($request['id']);
        $paginas->name = $request['name'];
        $paginas->url = $request['url'];
        $paginas->url_login = $request['url_login'];
        $paginas->category_id = $request['category_id'];
        $paginas->column_nick = strtoupper($request['column_nick']);
        $paginas->column_value = strtoupper($request['column_value']);
        $paginas->status_id = 2;
        $paginas->save();
        return $paginas;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function destroy(Page $page)
    {
        //
    }

    /**
     * Listado de pages para select
     */
    public function listIndex()
    {
        $data = DB::table('pages AS p')
            ->select('p.id', 'p.name', 'p.url')
            ->orderBy('p.name')
            ->get();

        return response()->json([
            'pages' => $data,
        ]);
    }
}
