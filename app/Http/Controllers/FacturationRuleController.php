<?php

namespace App\Http\Controllers;

use App\FacturationRule;
use App\Page;
use App\Sedes;
use App\Modelo;
use Illuminate\Http\Request;

class FacturationRuleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rule = FacturationRule::with('infomodelo')
                               ->with('sede')
                               ->with('pagina')
                               ->get();
        $pages = Page::get();
        $sedes = Sedes::get();
        $modelos = Modelo::get();

        return response()->json([
            'reglas' => $rule,
            'modelos' => $modelos,
            'sedes' => $sedes,
            'paginas' => $pages
        ]); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request = json_decode($request->getContent(), true);

        $rule = new FacturationRule();

        $rule->init_date    = $request["init_date"];
        $rule->minimun      = $request["minimun"];
        $rule->maximum      = $request["maximum"];
        $rule->percentage   = $request["percentage"];
        $rule->trm          = $request["trm"];
        $rule->page_id      = $request["page_id"];
        $rule->model_id     = $request["model_id"];
        $rule->sede_id      = $request["sede_id"];

        $rule->save(); 
        return $rule;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\FacturationRule  $facturationRule
     * @return \Illuminate\Http\Response
     */
    public function show(FacturationRule $facturationRule)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\FacturationRule  $facturationRule
     * @return \Illuminate\Http\Response
     */
    public function edit(FacturationRule $facturationRule)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\FacturationRule  $facturationRule
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $request = json_decode($request->getContent(), true);

        $rule = FacturationRule::find($request['id']);
        $rule->init_date    = $request["init_date"];
        $rule->minimun      = $request["minimun"];
        $rule->maximum      = $request["maximum"];
        $rule->percentage   = $request["percentage"];
        $rule->trm          = $request["trm"];
        $rule->page_id      = $request["page_id"];
        $rule->model_id     = $request["model_id"];
        $rule->sede_id      = $request["sede_id"];
        $rule->save();
        return $rule;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\FacturationRule  $facturationRule
     * @return \Illuminate\Http\Response
     */
    public function destroy(FacturationRule $facturationRule)
    {
        //
    }
}
