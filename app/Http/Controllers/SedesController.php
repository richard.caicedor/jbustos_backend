<?php

namespace App\Http\Controllers;

use App\Page;
use App\PagesSede;
use App\BasicInformation;
use App\Sedes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SedesController extends Controller
{ 
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sedes = Sedes::where('status_id', 2)->get();
        $bancos = BasicInformation::where('category_id',26)->get();
        $paginas = Page::get();
        return response()->json([
            'sedes' => $sedes,
            'bancos' => $bancos,
            'paginas' => $paginas
        ]); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request = json_decode($request->getContent(), true);
        $sedes = new Sedes();
        $sedes->name = $request['name'];
        $sedes->representative = $request['representative'];
        $sedes->phone = $request['phone'];
        $sedes->email = $request['email'];
        $sedes->type = $request['type'];
        $sedes->address = $request['address'];
        $sedes->banco_id = $request['banco_id'];
        $sedes->cuenta = $request['cuenta'];
        $sedes->nit = $request['nit'];
        $sedes->titular = $request['titular'];
        $sedes->tipo_pago = $request['tipo_pago'];
        $sedes->moneda = $request['moneda'];
        $sedes->numero = $request['numero'];
        $sedes->email_pago = $request['email_pago'];
        $sedes->status_id = 2;

        $sedes->save();

        $paginas = Page::get();

        foreach ($paginas as $key => $value) {
            $pageSede = new PagesSede();

            $pageSede->sede_id = $sedes->id;
            $pageSede->page_id = $value->id;
            $pageSede->status_id = 2;

            $pageSede->save();
        }
        return $sedes;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Sedes  $sedes
     * @return \Illuminate\Http\Response
     */
    public function show(Sedes $sedes)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Sedes  $sedes
     * @return \Illuminate\Http\Response
     */
    public function edit(Sedes $sedes)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Sedes  $sedes
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $request = json_decode($request->getContent(), true);
        $sedes = Sedes::find($request['id']);

        $sedes->name = $request['name'];
        $sedes->representative = $request['representative'];
        $sedes->phone = $request['phone'];
        $sedes->email = $request['email'];
        $sedes->type = $request['type'];
        $sedes->address = $request['address'];
        $sedes->banco_id = $request['banco_id'];
        $sedes->cuenta = $request['cuenta'];
        $sedes->nit = $request['nit'];
        $sedes->titular = $request['titular'];
        $sedes->tipo_pago = $request['tipo_pago'];
        $sedes->moneda = $request['moneda'];
        $sedes->numero = $request['numero'];
        $sedes->email_pago = $request['email_pago'];
        $sedes->status_id = 2;

        $sedes->save();
        return $sedes;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Sedes  $sedes
     * @return \Illuminate\Http\Response
     */
    public function destroy(Sedes $sedes)
    {
        //
    }

    /**
     * Listado de sedes
     */
    public function listIndex()
    {
        $data = DB::table('sedes AS s')
            ->select('s.id', 's.name')
            ->orderBy('s.name')
            ->get();

        return response()->json([
            'sedes' => $data,
        ]);
    }
}
