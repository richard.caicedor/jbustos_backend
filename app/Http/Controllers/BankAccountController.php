<?php

namespace App\Http\Controllers;

use App\BankAccount;
use Illuminate\Http\Request;

class BankAccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function createAccount($modelId, $cuenta, $propietario,$bancoId){

        $bankaccount = BankAccount::where('model_id', $modelId)
                                  ->where('status_id',21)
                                  ->get();

        $insertbank = 1;

        if($bankaccount->count() > 0){
            if($bankaccount[0]->account_number == $cuenta){
                $bankaccount[0]->own_name = $propietario;
                $bankaccount[0]->bank_id = $bancoId;
                $bankaccount[0]->save();
                $insertbank = 0;
            }else{
                $bankaccount[0]->status_id = 22;
                $bankaccount[0]->save();
            }            
        }

        if($insertbank == 1){
            $bankaccount = new BankAccount();
    
            $bankaccount->model_id = $modelId;
            $bankaccount->account_number = $cuenta;
            $bankaccount->own_name = $propietario;
            $bankaccount->bank_id = $bancoId;
            $bankaccount->status_id = 21;
    
            $bankaccount->save();
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\BankAccount  $bankAccount
     * @return \Illuminate\Http\Response
     */
    public function show(BankAccount $bankAccount)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\BankAccount  $bankAccount
     * @return \Illuminate\Http\Response
     */
    public function edit(BankAccount $bankAccount)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\BankAccount  $bankAccount
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BankAccount $bankAccount)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\BankAccount  $bankAccount
     * @return \Illuminate\Http\Response
     */
    public function destroy(BankAccount $bankAccount)
    {
        //
    }
}
