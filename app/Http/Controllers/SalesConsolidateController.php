<?php

namespace App\Http\Controllers;

use App\SalesConsolidate;
use App\Sales;
use App\FacturationRule;
use App\Sedes;
use App\Modelo;
use App\Page;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SalesConsolidateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function Liquidar()
    {
        //
        $initialDate = date('2020-01-01');
        $ventas = Sales::whereNull('consolidate_id')
                       ->where('sales_date','>=',$initialDate)
                       //->where('sales_date','<=',$initialDate)
                       //->orderBy('model_id asc', 'page_id asc')
                       ->get();

        $ventaTotal = 0;
        $ventaPagina = 0;
        $ventaTokens = 0;
        $ventaDollars = 0;
        $page_id = 0;
        $model_id = 0;
        $percentage = 0;
        foreach ($ventas as $venta) {

          //die();
          if($page_id != $venta->page_id){
            //var_dump($venta);
            if($page_id != 0){
              if($page_id = -1 ){
                $page_id = 1;
              }

            //Se valida si tiene regla para las ventas en la pagina y modelo
            $rule = FacturationRule::whereNull('final_date')
                                     ->where('page_id',$venta->page_id)
                                     ->where('model_id',$venta->model_id)
                                     ->where('minimun','>=', $ventaPagina)
                                     ->where('maximum','<=',$ventaPagina)
                                     ->first();


            //Se valida si se tiene regla para la modelo y página
            if(!isset($rule)){
              $rule = FacturationRule::whereNull('final_date')
                                     ->whereNull('minimun')
                                     ->whereNull('maximum')
                                     ->whereNull('sede_id')
                                     ->where('page_id',$venta->page_id)
                                     ->where('model_id',$venta->model_id)
                                     ->first();
            }


            //Se valida si se tiene regla para la modelo unicamente
            if(!isset($rule)){
              $rule = FacturationRule::whereNull('final_date')
                                     ->whereNull('minimun')
                                     ->whereNull('maximum')
                                     ->whereNull('sede_id')
                                     ->whereNull('page_id')
                                     ->where('model_id',$venta->model_id)
                                     ->first();
            }

            //Se valida si tiene regla para las ventas en la pagina y sede
            if(!isset($rule)){
              $rule = FacturationRule::whereNull('final_date')
                                      ->whereNull('model_id')
                                      ->where('page_id',$venta->page_id)
                                     ->where('sede_id',$venta->sede_id)
                                     ->where('minimun','>=', $ventaPagina)
                                     ->where('maximum','<=',$ventaPagina)
                                     ->first();
            }

            //Se valida si se tiene regla para la página y la sede
            if(!isset($rule)){
              $rule = FacturationRule::whereNull('final_date')
                                     ->whereNull('minimun')
                                     ->whereNull('maximum')
                                     ->whereNull('model_id')
                                     ->where('page_id',$venta->page_id)
                                     ->where('sede_id',$venta->sede_id)
                                     ->first();
            }

            //Se valida si tiene regla para las ventas en la pagina y sede
            if(!isset($rule)){
              $rule = FacturationRule::whereNull('final_date')
                                     ->whereNull('model_id')
                                     ->whereNull('sede_id')
                                     ->where('page_id',$venta->page_id)
                                     ->where('minimun','>=', $ventaPagina)
                                     ->where('maximum','<=',$ventaPagina)
                                     ->first();
            }

            //Se valida si se tiene regla para la sede unicamente
            if(!isset($rule)){
              $rule = FacturationRule::whereNull('final_date')
                                     ->whereNull('minimun')
                                     ->whereNull('maximum')
                                     ->whereNull('model_id')
                                     ->whereNull('page_id')
                                     ->where('sede_id',$venta->sede_id)
                                     ->first();
            }

            //Se valida si se tiene regla para la página unicamente
            if(!isset($rule)){
              $rule = FacturationRule::whereNull('final_date')
                                     ->whereNull('minimun')
                                     ->whereNull('maximum')
                                     ->whereNull('sede_id')
                                     ->whereNull('model_id')
                                     ->where('page_id',$venta->page_id)
                                     ->first();
            }

            // Si no se encuentra regla se escoge el porcentaje de la sede
            if(!isset($rule)){
              $sedeInfo = Sedes::find($venta->sede_id);
              $percentage = $sedeInfo->type;
            }else{
              $percentage = $rule->percentage;
            }

            $ventasConsolidada = new SalesConsolidate();

            $ventasConsolidada->total_paid        = $ventaPagina;
            $ventasConsolidada->total_dollars     = $ventaDollars;
            $ventasConsolidada->total_tokens      = $ventaTokens;
            $ventasConsolidada->percentage_paid   = $percentage;
            $ventasConsolidada->facturation_date  = date("Y-m-d");
            $ventasConsolidada->page_id           = $page_id;
            $ventasConsolidada->model_id          = $model_id;
            $ventasConsolidada->sede_id           = $venta->sede_id;
            if(isset($rule)){
              $ventasConsolidada->rule_id         = $rule->id;
            }

            $ventasConsolidada->save();
          }
             $page_id =  $venta->page_id;
             $ventaPagina = 0;
             $ventaTokens = 0;
             $ventaDollars = 0;

          }else{
             $ventaPagina = $ventaPagina + (($venta->tokens * 0.05) + $venta->dollars);
             $ventaTokens = $venta->tokens;
             $ventaDollars = $venta->dollars;

          }
            if($model_id != $venta->model_id){
              if($model_id != 0 ){
                //Se valida si se tiene regla para el rango de $ventas
                $rule = FacturationRule::whereNull('final_date')
                ->where('minimun',$venta->$ventaTotal)
                ->where('maximum',$venta->$ventaTotal)
                ->first();
                /*if(isset($rule)){
                  SalesConsolidate::where()
                }*/
              }

              $model_id = $venta->model_id;
              $ventaTotal = 0;
              $page_id = 0;
            //  $page_id = -1;
            }else{
              $ventaTotal = $ventaTotal + (($venta->tokens * 0.05) + $venta->dollars);
            }



        }

        
    }

    public function basicInfo()
    {
        $sedes = Sedes::get();
        $models = Modelo::get();
        
        return response()->json([
            'sedes' => $sedes,
            'modelos' => $models  
        ]);
    }

    public function LiquidarModelos(Request $request)
    {
        $requestInfo = json_decode($request->getContent(), true);

        // $reglas = DB::table('sales AS s')
        //             ->join('facturaction_rules AS fr')

        // $sedes = Sedes::whereIn('id',$requestInfo['sedes_id'])->get();

        // var_dump($sedes);
        // die();
        $initialDate = $requestInfo['fecha_fin'];
        $ventas = Sales::whereNull('consolidate_id')
                       ->whereIn('sede_id', $requestInfo['sedes_id'])
                       ->where('sales_date', '<', $initialDate)
                       ->select('page_id', 'sede_id', 'model_id', DB::raw('sum(tokens) as tokens'), DB::raw('sum(dollars) as dollars'))
                       ->groupBy('page_id', 'sede_id', 'model_id')
                       ->orderBy('model_id', 'asc')
                       ->orderBy('page_id', 'asc');

        
        if($requestInfo['modelo_id'] != null){
          $ventas = $ventas->whereIn('model_id', $requestInfo['modelo_id']);
        }

        $ventas = $ventas->get();

        $ventaTotal = 0;
        $ventaPagina = 0;
        $ventaTokens = 0;
        $ventaDollars = 0;
        $page_id = 0;
        $model_id = 0;
        $percentage = 0;
        foreach ($ventas as $venta) {

          //die();
          if($page_id != $venta->page_id){
            $page_id = $venta->page_id;
            $model_id = $venta->model_id;

            $pageInfo = Page::find($venta->page_id);
            $type = $pageInfo->category_id;
            if($type == 13){
              $salest = $venta->tokens;
            }else{
              $salest = $venta->dollars;
            }


            //Se valida si tiene regla para las ventas en la pagina y modelo
            $rule = FacturationRule::whereNull('final_date')
                                     ->where('page_id',$venta->page_id)
                                     ->where('model_id',$venta->model_id)
                                     ->where('sede_id',$venta->sede_id)
                                     ->where('minimun','>=', $salest)
                                     ->where('maximum','<=',$salest)
                                     ->first();

            if(!isset($rule)){
              //Se valida si tiene regla para las ventas en la pagina y modelo y sede
              $rule = FacturationRule::whereNull('final_date')
                                     ->where('page_id',$venta->page_id)
                                     ->where('model_id',$venta->model_id)
                                     ->where('sede_id',$venta->sede_id)
                                     ->whereNull('minimun')
                                     ->whereNull('maximum')
                                     ->first();
            }

            if(!isset($rule)){
              //Se valida si tiene regla para las ventas en la pagina y modelo y sede
              $rule = FacturationRule::whereNull('final_date')
                                     ->where('page_id',$venta->page_id)
                                     ->where('model_id',$venta->model_id)
                                     ->whereNull('sede_id')
                                     ->where('minimun','>=', $salest)
                                     ->where('maximum','<=',$salest)
                                     ->first();
            }

            //Se valida si se tiene regla para la modelo y página
            if(!isset($rule)){
              $rule = FacturationRule::whereNull('final_date')
                                     ->whereNull('minimun')
                                     ->whereNull('maximum')
                                     ->whereNull('sede_id')
                                     ->where('page_id',$venta->page_id)
                                     ->where('model_id',$venta->model_id)
                                     ->first();
            }


            //Se valida si se tiene regla para la modelo unicamente
            if(!isset($rule)){
              $rule = FacturationRule::whereNull('final_date')
                                     ->whereNull('minimun')
                                     ->whereNull('maximum')
                                     ->whereNull('sede_id')
                                     ->whereNull('page_id')
                                     ->where('model_id',$venta->model_id)
                                     ->first();
            }

            //Se valida si tiene regla para las ventas en la pagina y sede
            if(!isset($rule)){
              $rule = FacturationRule::whereNull('final_date')
                                      ->whereNull('model_id')
                                      ->where('page_id',$venta->page_id)
                                     ->where('sede_id',$venta->sede_id)
                                     ->where('minimun','>=', $salest)
                                     ->where('maximum','<=',$salest)
                                     ->first();
            }

            //Se valida si se tiene regla para la página y la sede
            if(!isset($rule)){
              $rule = FacturationRule::whereNull('final_date')
                                     ->whereNull('minimun')
                                     ->whereNull('maximum')
                                     ->whereNull('model_id')
                                     ->where('page_id',$venta->page_id)
                                     ->where('sede_id',$venta->sede_id)
                                     ->first();
            }

            //Se valida si tiene regla para las ventas en la pagina y sede
            if(!isset($rule)){
              $rule = FacturationRule::whereNull('final_date')
                                     ->whereNull('model_id')
                                     ->whereNull('sede_id')
                                     ->where('page_id',$venta->page_id)
                                     ->where('minimun','>=', $salest)
                                     ->where('maximum','<=',$salest)
                                     ->first();
            }

            //Se valida si se tiene regla para la sede unicamente
            if(!isset($rule)){
              $rule = FacturationRule::whereNull('final_date')
                                     ->whereNull('minimun')
                                     ->whereNull('maximum')
                                     ->whereNull('model_id')
                                     ->whereNull('page_id')
                                     ->where('sede_id',$venta->sede_id)
                                     ->first();
            }

            //Se valida si se tiene regla para la página unicamente
            if(!isset($rule)){
              $rule = FacturationRule::whereNull('final_date')
                                     ->whereNull('minimun')
                                     ->whereNull('maximum')
                                     ->whereNull('sede_id')
                                     ->whereNull('model_id')
                                     ->where('page_id',$venta->page_id)
                                     ->first();
            }

            // Si no se encuentra regla se escoge el porcentaje de la sede
            if(!isset($rule)){
              $sedeInfo = Sedes::find($venta->sede_id);
              $percentage = $sedeInfo->type;
            }else{
              $percentage = $rule->percentage;
            }

            if($type == 13){
              $totalpaid = ($salest * 0.05 * $percentage) / 100;
              $totaldollars = $salest * 0.05;
              $totaltokens = $salest;
            }else{
              $totalpaid = ($salest * $percentage) / 100;
              $totaldollars = $salest;
              $totaltokens = null;
            }
            
            $ventasConsolidada = new SalesConsolidate();

            $ventasConsolidada->total_paid        = $totalpaid;
            $ventasConsolidada->total_dollars     = $totaldollars;
            $ventasConsolidada->total_tokens      = $totaltokens;
            $ventasConsolidada->percentage_paid   = $percentage;
            $ventasConsolidada->facturation_date  = date('Y-m-d');            
            $ventasConsolidada->initial_date      = $initialDate;
            $ventasConsolidada->page_id           = $page_id;
            $ventasConsolidada->model_id          = $model_id;
            $ventasConsolidada->sede_id           = $venta->sede_id;
            if(isset($rule)){
              $ventasConsolidada->rule_id         = $rule->id;
            }

            $ventasConsolidada->save();
            
            DB::table('sales')
              ->where('model_id', $model_id)
              ->where('page_id', $page_id)
              ->where('sede_id', $venta->sede_id)
              ->whereNull('consolidate_id')
              ->update(['consolidate_id' => $ventasConsolidada->id]);

          }else{
             $ventaPagina = $ventaPagina + (($venta->tokens * 0.05) + $venta->dollars);
             $ventaTokens = $venta->tokens;
             $ventaDollars = $venta->dollars;

          }
            if($model_id != $venta->model_id){
              if($model_id != 0 ){
                //Se valida si se tiene regla para el rango de $ventas
                $rule = FacturationRule::whereNull('final_date')
                ->where('minimun',$ventaTotal)
                ->where('maximum',$ventaTotal)
                ->first();
                if(isset($rule)){
                  DB::table('sales_consolidates')
                    ->where('model_id', $model_id)
                    ->where('initial_date', $initialDate)
                    ->update(['rule_id' => $rule->id]);
                }
                /*if(isset($rule)){
                  SalesConsolidate::where()
                }*/
              }

              
              $ventaTotal = 0;
              $page_id = 0;
            }else{
              $ventaTotal = $ventaTotal + (($venta->tokens * 0.05) + $venta->dollars);
            }
        }

        return response()->json([
            'status'=> "Ok",
            'message' => 'Ok',
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SalesConsolidate  $salesConsolidate
     * @return \Illuminate\Http\Response
     */
    public function show(SalesConsolidate $salesConsolidate)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SalesConsolidate  $salesConsolidate
     * @return \Illuminate\Http\Response
     */
    public function edit(SalesConsolidate $salesConsolidate)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SalesConsolidate  $salesConsolidate
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SalesConsolidate $salesConsolidate)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SalesConsolidate  $salesConsolidate
     * @return \Illuminate\Http\Response
     */
    public function destroy(SalesConsolidate $salesConsolidate)
    {
        //
    }
}
