<?php

namespace App\Http\Controllers;

use App\Location;
use App\Categories;
use Illuminate\Http\Request;

class LocationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      //$location = Location->get();
      //return $location;
    }

    public function LocationByCategoria($category)
    {
      $locations = Location::where('category_id', $category)->get();
      $paises = Location::where('category_id', 15)->get();
      $departamentos = Location::where('category_id',16)->get();
      $municipios = Location::where('category_id',17)->get();
      return response()->json([
            'locations' => $locations,
            'paises' => $paises, 
            'departamentos' => $departamentos,
            'municipios' => $municipios,
        ]); 

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $request = json_decode($request->getContent(), true);
      $location = new Location();

      $location->description    = $request['description'];
      $location->parent_id      = $request['parent_id'];
      $location->category_id    = $request['category_id'];

      $location->save();
      return $location;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function show(Location $location)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function edit(Location $location)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
      $request = json_decode($request->getContent(), true);
      $location = Location::find($request['id']);

      $location->description    = $request['description'];
      $location->parent_id      = $request['parent_id'];
      $location->category_id    = $request['category_id'];

      $location->save();
      return $location;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function destroy(Location $location)
    {
        //
    }
}
