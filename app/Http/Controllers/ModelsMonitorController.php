<?php

namespace App\Http\Controllers;

use App\ModelsMonitor;
use Illuminate\Http\Request;

class ModelsMonitorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    public function getModelosMonitor($monitorid)
    {
        $modelInfoOld = ModelsMonitor::where('monitor_id', $monitorid)
                                     ->whereNull('final_date')->get();

        return response()->json([
            'status'=> "Ok",
            'datos'=> $modelInfoOld
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request = json_decode($request->getContent(), true);
        $insertnew = true;

        $modelInfoOld = ModelsMonitor::where('model_id', $request['model_id'])
                                     ->whereNull('final_date')->first();

        if($modelInfoOld != null){

            if($modelInfoOld->initial_date > $request['initial_date']){
                return response()->json([
                    status=> "Error",
                    message=> "La fecha de inicio debe ser mayor a la fecha de inicio del último asignamiento"
                ]);
            }
                $modelInfoOld->final_date = $request['initial_date'];
                $modelInfoOld->save();

        }

        if($insertnew){
            $modelsMonitor = new ModelsMonitor();
            $modelsMonitor->initial_date    = $request['initial_date'];
            $modelsMonitor->model_id        = $request['model_id']; 
            $modelsMonitor->monitor_id      = $request['monitor_id'];
            $modelsMonitor->percentage      = $request['percentage'];

            $modelsMonitor->save();
        }

        return response()->json([
            'status'=> "Ok",
            'message'=> "Se asigno la modelo al monitor"
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ModelsMonitor  $modelsMonitor
     * @return \Illuminate\Http\Response
     */
    public function show(ModelsMonitor $modelsMonitor)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ModelsMonitor  $modelsMonitor
     * @return \Illuminate\Http\Response
     */
    public function edit(ModelsMonitor $modelsMonitor)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ModelsMonitor  $modelsMonitor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ModelsMonitor $modelsMonitor)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ModelsMonitor  $modelsMonitor
     * @return \Illuminate\Http\Response
     */
    public function destroy($modelsMonitor)
    {
        //
        $monitorInfo = ModelsMonitor::find($modelsMonitor);
        $monitorInfo->delete();

        return response()->json([
            'status'=> "Ok",
            'message'=> "Registro Eliminado"
        ]);
    }
}
