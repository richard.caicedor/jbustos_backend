<?php

namespace App\Http\Controllers;

use App\Modelo;
use App\ModelsCouple;
use App\Nick;
use App\PagesSede;
use Illuminate\Http\Request;

class NickController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request = json_decode($request->getContent(), true);
        $modelo = Modelo::find($request["model_id"]);

        $page = PagesSede::where('page_id', $request['page_id'])
            ->where('sede_id', $modelo->sede_id)
            ->first();

        if($page != null){
            $nickinfo = Nick::where('nickname',$request['nickname'])
                            ->where('page_id',$request['page_id'])
                            ->first();
            if($nickinfo == null){
                $nicks = $modelo->nicks()->create([
                    'nickname' => $request['nickname'],
                    'password' => $request["password"],
                    'url'      => $request['url'],
                    'page_id'  => $request['page_id'],
                    'category_id' => 2,
                    'status_id'   => 2
                ]);
            }else{
                return response()->json([
                    'status'  => 'Error',
                    'message' => 'Registro Eliminado',
                ]);
            }
        }
        return $nicks;
    }

    public function addCoupleNick(Request $request)
    {
        $request = json_decode($request->getContent(), true);
        $modelo = ModelsCouple::find($request["model_id"]);
        $page = PagesSede::where('page_id', $request['page_id'])
            ->first();
        if ($page != null) {
            $nicks = $modelo->nickinfo()->create([
                'nickname' => $request['nickname'],
                'password' => $request["password"],
                'url' => $request['url'],
                'page_id' => $request['page_id'],
                'category_id' => 2,
                'status_id' => 2,
            ]);
        }
        return $nicks;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Nick  $nick
     * @return \Illuminate\Http\Response
     */
    public function show($nick)
    {
        $modelo = Modelo::find($nick);

        $nicks = $modelo->nicks()->with("pagina")->get();

        return $nicks;
    }

    public function showCoupleNick($nick)
    {
        $modelo = ModelsCouple::find($nick);

        $nicks = $modelo->nickinfo()->with("pagina")->get();

        return $nicks;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Nick  $nick
     * @return \Illuminate\Http\Response
     */
    public function edit(Nick $nick)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Nick  $nick
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $request = json_decode($request->getContent(), true);
        $nicks = Nick::find($request['id']);

        $modelo = Modelo::find($request['model_id']);

        $page = PagesSede::where('page_id', $request['page_id'])
            ->where('sede_id', $modelo->sede_id)
            ->first();

        $nicks->nickname = $request["nickname"];
        $nicks->password = $request["password"];
        $nicks->url = $request["url"];

        if ($page != null) {
            $nicks->page_id = $request['page_id'];
        }

        $nicks->save();
        return $nicks;
    }

    public function updateCoupleNick(Request $request)
    {
        $request = json_decode($request->getContent(), true);
        $nicks = Nick::find($request['id']);

        $modelo = ModelsCouple::find($request['model_id']);

        $page = PagesSede::where('page_id', $request['page_id'])
            ->where('sede_id', $modelo->modeloinfo->sede_id)
            ->first();

        $nicks->nickname = $request["nickname"];
        $nicks->password = $request["password"];
        $nicks->url = $request["url"];

        if ($page != null) {
            $nicks->page_id = $request['page_id'];
        }

        $nicks->save();
        return $nicks;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Nick  $nick
     * @return \Illuminate\Http\Response
     */
    public function destroy($nick)
    {
        $nicks = Nick::find($nick);
        $nicks->delete();
        return response()->json([
            'message' => 'Registro Eliminado',
        ]);
    }  
}
