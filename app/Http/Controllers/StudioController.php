<?php

namespace App\Http\Controllers;

use App\Studio;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class StudioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $studio = Studio::where('status_id', 2)->get();
        return $studio;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request = json_decode($request->getContent(), true);
        $studio = new Studio();
        $studio->name = $request['name'];
        $studio->representative = $request['representative'];
        $studio->contact_phone = $request['contact_phone'];
        $studio->status_id = 2;

        $studio->save();

        return $studio;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Studio  $studio
     * @return \Illuminate\Http\Response
     */
    public function show(Studio $studio)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Studio  $studio
     * @return \Illuminate\Http\Response
     */
    public function edit(Studio $studio)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Studio  $studio
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $request = json_decode($request->getContent(), true);
        $studio = Studio::find($request['id']);

        $studio->name = $request['name'];
        $studio->representative = $request['representative'];
        $studio->contact_phone = $request['contact_phone'];
        $studio->status_id = 2;

        $studio->save();
        return $studio;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Studio  $studio
     * @return \Illuminate\Http\Response
     */
    public function destroy($studio)
    {
        $studio = Studio::find($studio);
        $studio->delete();

        return $studio;
    }

    /**
     * Listado de sedes
     */
    public function listIndex()
    {
        $data = DB::table('studios AS s')
            ->select('s.id', 's.name')
            ->orderBy('s.name')
            ->get();

        return response()->json([
            'studios' => $data,
        ]);
    }
}
