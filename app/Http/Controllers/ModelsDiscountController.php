<?php

namespace App\Http\Controllers;

use App\models_discount;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ModelsDiscountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sedes = DB::table('sedes as s')
                   ->where('s.status_id', 21)
                   ->get();

        return $sedes;
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $modelDiscountInfo = json_decode($request->getContent(), true);
        $cant = count($modelDiscountInfo);
        $modelFilters = $modelDiscountInfo[$cant-1];
        array_pop($modelDiscountInfo);

        foreach ($modelDiscountInfo as $key => $modelInfo) {

            $insertDiscount = 0;

            $modelsDiscount = new models_discount();
            $modelsDiscount->model_id       = $modelInfo['model_id']; 
            if($modelInfo['arriendo'] != 0 && $modelInfo['arriendo'] != null){
                $modelsDiscount->arriendo       = $modelInfo['arriendo'];
                $insertDiscount = 1;
            }
            if($modelInfo['internet'] != 0 && $modelInfo['internet'] != null){
                $modelsDiscount->internet       = $modelInfo['internet'];
                $insertDiscount = 1;
            }
            if($modelInfo['servicio'] != 0 && $modelInfo['servicio'] != null){
                $modelsDiscount->servicios      = $modelInfo['servicio'];
                $insertDiscount = 1;
            }
            if($modelInfo['borratusvideos'] != 0 && $modelInfo['borratusvideos'] != null){
                $modelsDiscount->borratusvideos = $modelInfo['borratusvideos'];
                $insertDiscount = 1;
            }
            if($modelInfo['prestamos'] != 0 && $modelInfo['prestamos'] != null){
                $modelsDiscount->prestamos      = $modelInfo['prestamos'];
                $insertDiscount = 1;
            }
            if($modelInfo['eps'] != 0 && $modelInfo['eps'] != null){
                $modelsDiscount->eps            = $modelInfo['eps'];
                $insertDiscount = 1;
            }
            if($modelInfo['descuentos'] != 0 && $modelInfo['descuentos'] != null){
                $modelsDiscount->descuentos     = $modelInfo['descuentos'];
                $insertDiscount = 1;
            }
            if($modelInfo['premios'] != 0 && $modelInfo['premios'] != null){
                $modelsDiscount->premios        = $modelInfo['premios'];
                $insertDiscount = 1;
            }
            $modelsDiscount->observacion    = $modelInfo['observacion'];
            $modelsDiscount->initial_date   = $modelFilters['fecha_inicio'];
            $modelsDiscount->final_date     = $modelFilters['fecha_fin'];


            if ($insertDiscount == 1){
                $modelsDiscount->save();
            }
        }

        return response()->json([
            'status'=> "Ok",
            'message' => 'Descuentos almacenados',
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\models_discount  $models_discount
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $modelDiscountInfo = json_decode($request->getContent(), true);
        $info = DB::table("models AS m")
                  ->leftJoin("models_discounts AS md", function ($join) use ($modelDiscountInfo){
                      $join->on("md.model_id", "=","m.id")
                           ->where("md.initial_date",">=",$modelDiscountInfo["fecha_inicio"])
                           ->where("md.initial_date","<=",$modelDiscountInfo["fecha_fin"])
                           ->where("m.sede_id",$modelDiscountInfo["sede_id"]);
                  })
                  ->select("m.id AS model_id",
                           "m.identification AS identificacion",
                           DB::raw("concat(m.name,' ',m.lastname) nombre"),
                           "md.arriendo",
                           "md.internet",
                           "md.servicios AS servicio",
                           "md.borratusvideos",
                           "md.prestamos",
                           "md.eps",
                           "md.descuentos",
                           "md.premios",
                           "md.observacion")
                  ->where("m.status_id",13)
                  ->get();

        return $info;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\models_discount  $models_discount
     * @return \Illuminate\Http\Response
     */
    public function edit(models_discount $models_discount)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\models_discount  $models_discount
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $modelDiscountInfo = json_decode($request->getContent(), true);
        $cant = count($modelDiscountInfo);
        $modelFilters = $modelDiscountInfo[$cant-1];

        for ($i=0; $i <  $cant-1; $i++) {
            
            $insertDiscount = 0;

            $modelsDiscount = models_discount::find($modelDiscountInfo['id']);
            $modelsDiscount->model_id       = $modelDiscountInfo['model_id'];
            if($modelDiscountInfo['arriendo'] != 0 && $modelDiscountInfo['arriendo'] != null){
                $modelsDiscount->arriendo       = $modelDiscountInfo['arriendo'];
                $insertDiscount = 1;
            }
            if($modelDiscountInfo['internet'] != 0 && $modelDiscountInfo['internet'] != null){
                $modelsDiscount->internet       = $modelDiscountInfo['internet'];
                $insertDiscount = 1;
            }
            if($modelDiscountInfo['servicios'] != 0 && $modelDiscountInfo['servicios'] != null){
                $modelsDiscount->servicios      = $modelDiscountInfo['servicios'];
                $insertDiscount = 1;
            }
            if($modelDiscountInfo['borratusvideos'] != 0 && $modelDiscountInfo['borratusvideos'] != null){
                $modelsDiscount->borratusvideos = $modelDiscountInfo['borratusvideos'];
                $insertDiscount = 1;
            }
            if($modelDiscountInfo['prestamos'] != 0 && $modelDiscountInfo['prestamos'] != null){
                $modelsDiscount->prestamos      = $modelDiscountInfo['prestamos'];
                $insertDiscount = 1;
            }
            if($modelDiscountInfo['eps'] != 0 && $modelDiscountInfo['eps'] != null){
                $modelsDiscount->eps            = $modelDiscountInfo['eps'];
                $insertDiscount = 1;
            }
            if($modelDiscountInfo['descuentos'] != 0 && $modelDiscountInfo['descuentos'] != null){
                $modelsDiscount->descuentos     = $modelDiscountInfo['descuentos'];
                $insertDiscount = 1;
            }
            if($modelDiscountInfo['premios'] != 0 && $modelDiscountInfo['premios'] != null){
                $modelsDiscount->premios        = $modelDiscountInfo['premios'];
                $insertDiscount = 1;
            }
            $modelsDiscount->observacion    = $modelDiscountInfo['observacion'];
            $modelsDiscount->initial_date   = $modelFilters['initial_date'];
            $modelsDiscount->final_date     = $modelFilters['final_date'];


            if ($insertDiscount == 1){
                $modelsDiscount->save();
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\models_discount  $models_discount
     * @return \Illuminate\Http\Response
     */
    public function destroy($models_discount)
    {
        $modelsDiscount = models_discount::find($models_discount);
        $modelsDiscount->delete();
    }
}
