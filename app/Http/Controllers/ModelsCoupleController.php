<?php

namespace App\Http\Controllers;

use App\ModelsCouple;
use App\Modelo;
use App\Page;
use Illuminate\Http\Request;

class ModelsCoupleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $modelsCouple = ModelsCouple::where('status_id', 13)->with("modeloinfo")->with("pareja")->get();
        $modelos = Modelo::get();
        $pages = Page::get();

        return response()->json([
            'parejas' => $modelsCouple,
            'modelos' => $modelos,
            'paginas' => $pages
        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request = json_decode($request->getContent(), true);
        $modelsCouple = new ModelsCouple();

        $modelsCouple->model_id   = $request['model_id'];
        $modelsCouple->couple_id  = $request['couple_id'];
        $modelsCouple->status_id  = 13;
        $modelsCouple->save();

        return $modelsCouple;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ModelsCouple  $modelsCouple
     * @return \Illuminate\Http\Response
     */
    public function show(ModelsCouple $modelsCouple)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ModelsCouple  $modelsCouple
     * @return \Illuminate\Http\Response
     */
    public function edit(ModelsCouple $modelsCouple)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ModelsCouple  $modelsCouple
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $request = json_decode($request->getContent(), true);
        $modelsCouple = ModelsCouple::find($request['id']);

        $modelsCouple->model_id   = $request['model_id'];
        $modelsCouple->couple_id  = $request['couple_id'];
        $modelsCouple->save();
        return $modelsCouple;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ModelsCouple  $modelsCouple
     * @return \Illuminate\Http\Response
     */
    public function destroy($modelsCouple)
    {
        $modelsCouple = ModelsCouple::find($modelsCouple);
        $modelsCouple->delete();

        return $modelsCouple;
    }
}
