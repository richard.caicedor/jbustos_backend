<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RolActions extends Model
{
    //
    protected $fillable = [
        'name',
        'status_id',
    ];

    protected $hidden = [
        'created_by',
        'deleted_by',
    ];

}
