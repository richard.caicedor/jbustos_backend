<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Request extends Model
{
    //
    protected $fillable = [
		'nickname_suggest',
		'observation',
		'page_id',
		'sede_id_orig',
		'sede_id_dest',
		'model_id',
		'model_couple_id',
		'status_id'
    ];

    protected $hidden = [
		'created_by',
		'deleted_by'
    ];

    public function status()
    {
        return $this->belongsTo(Statuses::class, 'status_id', 'id');
    }

    public function creador()
    {
        return $this->belongsTo(User::class, 'created_by', 'id');
    }

    public function eliminador()
    {
        return $this->belongsTo(User::class, 'deleted_by', 'id');
    }

    public function page()
    {
        return $this->belongsTo(Page::class, 'page_id', 'id');
    }

    public function sedeOrig()
    {
        return $this->belongsTo(Sede::class, 'sede_id_orig', 'id');
    }

    public function sedeDest()
    {
        return $this->belongsTo(Sede::class, 'sede_id_dest', 'id');
    }

    public function modelo()
    {
        return $this->belongsTo(Model::class, 'model_id', 'id');
    }

    public function pareja()
    {
        return $this->belongsTo(ModelsCouple::class, 'model_couple_id', 'id');
    }

    public function files()
    {
        return $this->morphMany(UploadFile::class, 'uploadable');
    }

    public function historicalChanges()
    {
        return $this->morphMany(HistoricalChange::class, 'changeable');
    }
}
