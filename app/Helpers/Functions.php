<?php

namespace App\Helpers;

use App\Models\portfolio\AccountingDocTypeModel;
// use Exception;
use Carbon\Carbon;
use JWTAuth;
use Tymon\JWTAuth\Facades\JWTFactory;

class Fns
{
    const QUERY_CODE_DUPLICATED = 1062;
    /**
     * Generate token
     *
     * @param array $claims
     * @return string token
     */
    private static function generateToken($claims)
    {
        $claims = array_merge($claims, [
            'iss' => config('app.url'),
        ]);
        $factory = JWTFactory::customClaims($claims);
        $payload = $factory->make();
        return JWTAuth::encode($payload);
    }

    public static function generateGuestLink($type, $data)
    {
        $token = self::generateToken([
            'exp' => Carbon::now()->addDays(30)->timestamp,
            'data' => $data,
            'type' => $type,
        ]);

        return config('app.front_url') . "/auth/guest?t=" . $token;
    }

    public static function errorResponse($type, $data = null, $status = 400)
    {
        return response()->json([
            'type' => $type,
            'data' => $data,
        ], $status);
    }

    /**
     * Checks if a string starts with a string
     */
    public static function startsWith($haystack, $needle)
    {
        $length = strlen($needle);
        return (substr($haystack, 0, $length) === $needle);
    }

    /**
     * Remove unwanted accents
     *
     * @param string $str
     * @return string
     */
    public static function removeAccents($str)
    {
        $accents = ['Š' => 'S', 'š' => 's', 'Ž' => 'Z', 'ž' => 'z', 'À' => 'A', 'Á' => 'A', 'Â' => 'A', 'Ã' => 'A',
            'Ä' => 'A', 'Å' => 'A', 'Æ' => 'A', 'Ç' => 'C', 'È' => 'E', 'É' => 'E', 'Ê' => 'E', 'Ë' => 'E', 'Ì' => 'I',
            'Í' => 'I', 'Î' => 'I', 'Ï' => 'I', 'Ñ' => 'N', 'Ò' => 'O', 'Ó' => 'O', 'Ô' => 'O', 'Õ' => 'O', 'Ö' => 'O',
            'Ø' => 'O', 'Ù' => 'U', 'Ú' => 'U', 'Û' => 'U', 'Ü' => 'U', 'Ý' => 'Y', 'Þ' => 'B', 'ß' => 'Ss', 'à' => 'a',
            'á' => 'a', 'â' => 'a', 'ã' => 'a', 'ä' => 'a', 'å' => 'a', 'æ' => 'a', 'ç' => 'c', 'è' => 'e', 'é' => 'e',
            'ê' => 'e', 'ë' => 'e', 'ì' => 'i', 'í' => 'i', 'î' => 'i', 'ï' => 'i', 'ð' => 'o', 'ñ' => 'n', 'ò' => 'o',
            'ó' => 'o', 'ô' => 'o', 'õ' => 'o', 'ö' => 'o', 'ø' => 'o', 'ù' => 'u', 'ú' => 'u', 'û' => 'u', 'ý' => 'y',
            'þ' => 'b', 'ÿ' => 'y'];
        return strtr($str, $accents);
    }

    /**
     * Remove non valid characters for a filename
     */
    public static function removeNonValidFilenameCharacters($str)
    {
        $toRemove = ['/' => '', '\\' => '', '?' => '', '%' => '', '*' => '', ':' => '', '|' => '', '"' => '', '<' => '', '>' => ''];
        return strtr($str, $toRemove);
    }

    public static function getLikeFilter($text)
    {
        return '%' . str_replace(' ', '%', $text) . '%';
    }
}
