<?php

namespace App\Helpers;

class Pager
{
    public $size;
    public $filter;
    public $page;
    public $orderBy;
    public $order;
    public $skip;
    public $filters;

    public function __construct($input)
    {
        $this->size = (isset($input->s)) ? intval($input->s) : 10;
        $this->filter = (isset($input->f)) ? $input->f : '';
        $this->page = (isset($input->p)) ? intval($input->p) : 1;
        $this->filter = (isset($input->f)) ? $input->f : '';
        $this->orderBy = (isset($input->ob)) ? $this->camelCaseToUnderscoreCase($input->ob) : '';
        $this->order = (isset($input->o)) ? $input['o'] : 'asc';
        $this->skip = $this->size * ($this->page - 1);

        $this->filters = [];
        foreach ($input as $key => $value) {
            if (substr($key, 0, 2) === "f_") {
                $this->filters[substr($key, 2)] = $input[$key];
            }
        }
        $this->filters = (object) $this->filters;
    }

    public function hasFilter($column = '')
    {
        if ($column) {
            return isset($this->filters->{$column}) && $this->filters->{$column} !== '';
        } else {
            return $this->filter !== '';
        }
    }

    public function getLikeFilter($column = '')
    {
        return Fns::getLikeFilter($column ? $this->filters->{$column} : $this->filter);
    }

    public function isTrue($column)
    {
        return filter_var($this->filters->{$column}, FILTER_VALIDATE_BOOLEAN);
    }

    public function hasOrder()
    {
        return $this->orderBy !== '';
    }

    public function fixOrder($orders)
    {
        if (!$this->orderBy) {
            return;
        }

        foreach ($orders as $key => $value) {
            if ($this->orderBy && $this->orderBy == $key) {
                $this->orderBy = $value;
            }
        }
    }

    private function camelCaseToUnderscoreCase($input)
    {
        preg_match_all('!([A-Z][A-Z0-9]*(?=$|[A-Z][a-z0-9])|[A-Za-z][a-z0-9]+)!', $input, $matches);
        $ret = $matches[0];
        foreach ($ret as &$match) {
            $match = $match == strtoupper($match) ? strtolower($match) : lcfirst($match);
        }
        return implode('_', $ret);
    }
}
