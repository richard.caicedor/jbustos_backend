<?php

namespace App\Exports;

use App\SalesConsolidate;
use App\Modelo;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Support\Facades\DB;

class SalesConsolidateExport implements FromView
{
    
    private $modelId;
    private $fechaliquida;

    public function __construct($modelId, $fechaliquida)
    {
        $this->modelId = $modelId;
        $this->fechaliquida = $fechaliquida;
    }
    
    /**
    * @return \Illuminate\Support\Collection
    */
    public function view(): View
    {
        $modelInfo = Modelo::find($this->modelId);
        $modelName = $modelInfo->name.' '.$modelInfo->lastname;
        $paginas = DB::table('sales_consolidates AS sc')
                     ->join('pages AS p', 'sc.page_id', '=', 'p.id')
                     ->join('sales AS s', 'sc.id', '=', 's.consolidate_id')
                     ->join('nicks AS n', 's.nick_id', '=', 'n.id')
                     ->where('sc.initial_date', $this->fechaliquida )
                     ->where('sc.model_id', $modelInfo->id)
                     ->where('p.id', 's.page_id')
                    // ->select('p.name','n.nickname', 's.tokens', 's.dollars', 'sc.percentage_paid', DB::)
                     ->get();
        return view('recibo', [
            'invoices' => Invoice::all()
        ]);
    }
}
