<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class models_discount extends Model
{
    //
    protected $fillable = [
        'model_id',
        'arriendo',
        'internet',
        'servicios',
        'borratusvideos',
        'prestamos',
        'eps',
        'descuentos',
        'premios',
        'observacion',
        'initial_date',
        'final_date'
    ];

    protected $hidden = [
		'created_by',
		'deleted_by'
    ];

    public function creador()
    {
        return $this->belongsTo(User::class, 'created_by', 'id');
    }

    public function eliminador()
    {
        return $this->belongsTo(User::class, 'deleted_by', 'id');
    }

    public function modeloinfo()
    {
        return $this->belongsTo(Modelo::class, 'model_id', 'id');
    }
}
