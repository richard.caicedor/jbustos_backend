CREATE TABLE `menu` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `url` varchar(100) DEFAULT NULL,
  `icon` varchar(45) DEFAULT NULL,
  `order` tinyint(4) NOT NULL,
  `menu_id_parent` int(10) unsigned DEFAULT NULL,
  `action_id` bigint(20) unsigned DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_menu_id_parent_menu_idx` (`menu_id_parent`),
  KEY `fk_action_id_menu_idx` (`action_id`),
  CONSTRAINT `fk_menu_id_parent_menu` FOREIGN KEY (`menu_id_parent`) REFERENCES `menu` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_action_id_menu` FOREIGN KEY (`action_id`) REFERENCES `actions` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=93 DEFAULT CHARSET=utf8;


ALTER TABLE `actions` 
ADD COLUMN `name` VARCHAR(50) NOT NULL AFTER `id`,
ADD UNIQUE INDEX `name_UNIQUE` (`name` ASC);


-- ACCIONES

insert into actions values
(null, 'roles_view', 'Ver roles', '', 1, now(), now()),
(null, 'roles_create', 'Crear rol', '', 1, now(), now()),
(null, 'roles_edit', 'Editar rol', '', 1, now(), now()),
(null, 'roles_delete', 'Eliminar rol', '', 1, now(), now())
(null, 'roles_actions', 'Asignar acciones a rol', '', 1, now(), now());

insert into actions values
(null, 'actions_view', 'Ver acciones', '', 1, now(), now()),
(null, 'actions_create', 'Crear acción', '', 1, now(), now()),
(null, 'actions_edit', 'Editar acción', '', 1, now(), now()),
(null, 'actions_delete', 'Eliminar acción', '', 1, now(), now());

insert into menu values
(null, 'Acciones', 'actions', null, 7, (select m.id from menu m where m.name='Maestros'), (select a.id from actions a where a.name='actions_view'), now(), now());

insert into actions values
(null, 'users_view', 'Ver usuarios', '', 1, now(), now()),
(null, 'users_create', 'Crear usuario', '', 1, now(), now()),
(null, 'users_edit', 'Editar usuario', '', 1, now(), now()),
(null, 'users_delete', 'Eliminar usuario', '', 1, now(), now());

insert into menu values
(null, 'Usuarios', 'users', null, 8, (select m.id from menu m where m.name='Maestros'), (select a.id from actions a where a.name='users_view'), now(), now());

-- 28-02-2020 -----

insert into actions values
(null, 'menu_view', 'Ver menu', '', 1, now(), now()),
(null, 'menu_create', 'Crear menu', '', 1, now(), now()),
(null, 'menu_edit', 'Editar menu', '', 1, now(), now()),
(null, 'menu_delete', 'Eliminar menu', '', 1, now(), now());

insert into menu values
(null, 'Menu', 'menu', null, 8, (select m.id from menu m where m.name='Maestros'), (select a.id from actions a where a.name='menu_view'), now(), now());