<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModelsDiscountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('models_discounts', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->bigInteger('model_id')->unsigned();
            $table->decimal('arriendo',10,2)->nullable();
            $table->decimal('internet',10,2)->nullable();
            $table->decimal('servicios',10,2)->nullable();
            $table->decimal('borratusvideos',10,2)->nullable();
            $table->decimal('prestamos',10,2)->nullable();
            $table->decimal('eps',10,2)->nullable();
            $table->decimal('descuentos',10,2)->nullable();
            $table->decimal('premios',10,2)->nullable();
            $table->string('observacion')->nullable();
            $table->date('initial_date')->nullable();
            $table->date('final_date')->nullable();
            $table->bigInteger('created_by')->unsigned()->nullable();
            $table->bigInteger('deleted_by')->unsigned()->nullable();

            $table->foreign('model_id')->references('id')->on('models');
            $table->foreign('created_by')->references('id')->on('users');
            $table->foreign('deleted_by')->references('id')->on('users');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('models_discounts');
    }
}
