<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsMonitorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('monitors', function (Blueprint $table) {
          $table->date('birth_date')->nullable();
          $table->bigInteger('birth_country')->unsigned()->nullable();
          $table->bigInteger('birth_city')->unsigned()->nullable();
          $table->bigInteger('fdp_id')->unsigned()->nullable();
          $table->foreign('fdp_id')->references('id')->on('basic_information');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
