<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('users', function (Blueprint $table) {
        $table->bigInteger('sede_id')->unsigned()->nullable();
        $table->bigInteger('studio_id')->unsigned()->nullable();

        $table->foreign('sede_id')->references('id')->on('sedes');
        $table->foreign('studio_id')->references('id')->on('studios');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
