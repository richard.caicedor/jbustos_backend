<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRolActionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rol_actions', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->bigInteger('rol_id')->unsigned();
            $table->bigInteger('action_id')->unsigned();

            $table->foreign('rol_id')->references('id')->on('roles');
            $table->foreign('action_id')->references('id')->on('actions');

            $table->unique('rol_id', 'action_id');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rol_actions');
    }
}
