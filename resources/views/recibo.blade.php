<style>
	th{
		border: 1px solid #000;
	}

	td{
		border: 1px solid #000;
	}	
</style>
<table>
	<thead>
		<tr>
			<th colspan="8" align="center" valign="middle">{{$modelName}}</th>
		</tr>		
	</thead>
	<tbody>
		<tr>
			<th align="center" valign="middle">FECHA:</th>
			<td align="center" valign="middle">{{$fechaliquida}}</td>
			<th align="center" valign="middle">MONEDA PAGO:</th>
			<td align="center" valign="middle">COP</td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<th colspan="8" align="center" valign="middle">Calculo pagos</th>
		</tr>
		<tr>
			<th></th>
			<th>Pagina</th>
			<th>Nick</th>
			<th>Tokens</th>
			<th>Tokens en USD</th>
			<td>%Pago</td>
			<th>Total USD</th>
			<th></th>
		</tr>
		@foreach($paginas as $pagina)
			<tr>
				<th></th>
				<td>{{$pagina['nombre']}}</td>
				<td>{{$pagina['nick']}}</td>
				<td>{{$pagina['tokens']}}</td>
				<td>{{$pagina['usd']}}</td>
				<td>{{$pagina['percentage']}}</td>
				<td>{{$pagina['total']}}</td>
				<td></td>
			</tr>
		@endforeach()
	</tbody>	
</table>